# -*- coding: utf-8 -*-

import wx

# 最大学習時間の入力
def inputMaxTrainSec():
	dlg = wx.NumberEntryDialog(None, u'最大学習時間(秒)を入力してください', u'0:すべて', u'学習', 0, 0, 9999)
	r = dlg.ShowModal()
	dlg.Destroy()
	
	if r != wx.ID_OK:
		return -1
	return dlg.GetValue()

# 最大時間の入力
def inputMaxPredictSec():
	dlg = wx.NumberEntryDialog(None, u'最大予測時間(秒)を入力してください', u'0:すべて', u'予測', 0, 0, 9999)
	r = dlg.ShowModal()
	dlg.Destroy()
	
	if r != wx.ID_OK:
		return -1
	return dlg.GetValue()

# 最大シーン数の入力
def inputMaxScene():
	dlg = wx.NumberEntryDialog(None, u'最大シーンチェンジを入力してください', u'1以上', u'予測', 5, 1, 9999)
	r = dlg.ShowModal()
	dlg.Destroy()
	
	if r != wx.ID_OK:
		return -1
	return dlg.GetValue()


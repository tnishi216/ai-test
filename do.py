#
#
# gui1.pyの実行

import cv2
import numpy as np
import time
import random
from socket import gethostname

# C:/temp/test1.mp4
# 5:28 PG IN
# 59:28 CM IN
# 2:59:28 PG IN

# for learning
import tensorflow as tf

# for debug
import matplotlib.pyplot as plt

# Internal modules
from marklist import MarkList

# Logger
from logger import Logger
logger = Logger('do')

## Configuration ##

TargetFileNumber = '1'

TargetMp4File = "test"+TargetFileNumber+".mp4"
TargetMarkListFile = "marklist"+TargetFileNumber+".txt"

#tf.logging.set_verbosity(tf.logging.WARN)	# Ver.1.13以降？

import os
os.environ['TF_CPP_MIN_LOG_LEVEL'] = '2'

LOG_ENABLED = False

# 学習用画像のサイズ
imageWidth = 56
imageHeight = 56

# 元画像のサイズ
orgWidth = imageWidth
orgHeight = imageHeight

imageChannel = 3
SHRINK_IMAGE = True
BATCH_SIZE = 100
INPUT_SIZE = 200

imageViewWidth = 278
imageViewHeight = 146

# Thumbnail size
tbWidth = 139
tbHeight = 73

# working values
pendingCount = 0

# 最大学習・予測時間
maxSec = 0	# 0 means all

# Ref.: https://www.tensorflow.org/tutorials/estimators/cnn
# org github: https://github.com/tensorflow/tensorflow/blob/master/tensorflow/examples/tutorials/layers/cnn_mnist.py

# 参考：https://www.pyimagesearch.com/2017/02/06/faster-video-file-fps-with-cv2-videocapture-and-opencv/ (decode threading)

class MyApp:

	def __init__(self, *args, **kw):
		super(MyApp, self).__init__(*args, **kw)
		self.learnInitialized = False
		self.bitmapImage = None

		global TargetMp4File
		global TargetMarkListFile
		TargetMp4File = "test"+TargetFileNumber+".mp4"
		TargetMarkListFile = "marklist"+TargetFileNumber+".txt"

		self.PosList = []
		self.TypeList = {}

		self.PredPosList = []
		self.PredTypeList = {}

		# File operation
		self.edited = False
		self.filename = TargetMarkListFile

		self.load(None)

		self.loadMarkList()

	def load(self, event):
		if gethostname() == 'nishikawat-PC':
			FileName = "R:/temp/prs59i.mp4"		# 480x270 x3
		else:
			#FileName = "Q:/cap/test1.mp4"		# 268x148 x3
			FileName = "./image/"+TargetMp4File	# 268x148 x3
			# FileName = "Q:/cap/amarecco20180415-173633[000].avi"
		if not os.path.exists(FileName):
			logger.error("No file: " + FileName)
			exit()
		self.cap = cv2.VideoCapture(FileName)
		global imageWidth, imageHeight
		global orgWidth, orgHeight
		orgWidth = int(self.cap.get(cv2.CAP_PROP_FRAME_WIDTH))
		orgHeight = int(self.cap.get(cv2.CAP_PROP_FRAME_HEIGHT))
		if not SHRINK_IMAGE:
			imageWidth = orgWidth
			imageHeight = orgHeight
		
		# Show Info
		text = "%d x %d %dfps" % (orgWidth, orgHeight, self.fps())
		logger.info(text)
		
		# Resize controls
		global tbWidth, tbHeight
		tbWidth = orgWidth / 2
		tbHeight = orgHeight / 2

	def loadMarkList(self):
		if self.filename != '':
			marklist = MarkList()
			marklist.load(self.filename)
			self.PosList = marklist.frames
			self.TypeList = marklist.types

	def predict(self, event):
		#self.evalProc(self.getPos())
		self.predictProc(self.getPos())

	# 全frameから変化点を一覧する
	#TODO: bugがある！keras_cnn.pyのpredictAll()を参照
	def predictAll(self):
		SKIP_FRAMES = 15
	
		# Initialize
		self.PredPosList = []
		self.PredTypeList = {}
		self.PredPosList.clear()
		predlist = MarkList();

		count = 0

		start_time = time.time()

		self.initLearn()
		tf.logging.set_verbosity(tf.logging.WARN)
		prev_type = 0
		start_pos = 0
		for pos in range(0, self.getFrameCount(), SKIP_FRAMES):
			if maxSec != 0 and pos >= maxSec * self.fps():
				break

			type = self.predictFrame(pos)
			if type == 0:
				break

#			print("pos=%d type=%d" % (pos, type))

			if prev_type != type:
				if prev_type != 0:
					# 先頭以外
					for i in (start_pos+1, pos):
						# 正確な場所を調べる
#						print("i=",i)
						t = self.predictFrame(i)
						if t == 0:
							logger.critical("??? BUG???")
							break
						if t == type:
							# 正確な位置
							pos = i
							break

#				print("---------------------------------------")
				if start_pos == 0 and pos != 0:
					predlist.frames.append(0)
					predlist.types[0] = 2

				percent = int(pos * 100 / self.getFrameCount())
				logger.info("-- %s(%d/%d %d%%) %d" % (pos2str(pos), pos, self.getFrameCount(), percent, type))
				start_pos = pos
				prev_type = type
#				input('Next to push any key')

				self.PredPosList.append(start_pos)
				self.PredTypeList[pos] = type
				predlist.frames.append(start_pos)
				predlist.types[pos] = type
				count += 1
#				if count == 2:
#					break

		elapsed_time = time.time() - start_time

		predlist.save('predlist.txt')

#		self.updatePredImageList()

		logger.info("-- End of predictAll (%d[sec])" % (elapsed_time))

	# 半日かけて意味のないものを作ってしまった・・・
	def predictAllBS(self, event):
		self.PredPosList = []
		self.PredTypeList = {}
		self.PredPosList.clear()

		posTypeMap = {}

		found = 0
		not_found = 0	# 連続して見つからなかった回数
		old_found = 0	# 
		same_found = 0	# 連続して同じscene change frameが見つかった回数
		MaxSameFound = 10
		prev_found_pos = -1

		self.initLearn()
		tf.logging.set_verbosity(tf.logging.WARN)
		left = 0
		right = self.getFrameCount() - 1
		while True:
#			print("left=%d right=%d" % (left, right))
			if right <= left + 1:
				if posTypeMap[left] != posTypeMap[right]:
					# found
					pos = right
					if not pos in self.PredTypeList:
						# new found
						found += 1
						print("-- new found pos=%d type=%d scene=%d" % (pos, posTypeMap[pos], found))
						self.PredPosList.append(pos)
						self.PredTypeList[pos] = posTypeMap[pos]
						if found >= maxScene:
							# すべて見つけた
							#TODO: self.PredTypeListを見て、不連続なところだけやり直す
							break
						old_found = 0
					else:
						print("-- old found pos=%d type=%d same_found=%d" % (pos, posTypeMap[pos], same_found))
						old_found += 1
						if prev_found_pos == pos:
							same_found += 1
							if same_found >= MaxSameFound:
								print("!! too many same found")
								break
						else:
							same_found = 0
					prev_found_pos = pos
					not_found = 0
				else:
					not_found += 1
					same_found = 0
					print("-- Not found: left=%d right=%d" % (left, right))
				left = 0
				right = self.getFrameCount() - 1
				continue
			mid = int((right + left) / 2)
			if not left in posTypeMap:
				posTypeMap[left] = self.predictFrame(left)
			if not right in posTypeMap:
				posTypeMap[right] = self.predictFrame(right)
			if not mid in posTypeMap:
				posTypeMap[mid] = self.predictFrame(mid)
			# 異なるtypeのほうへ進める
			if posTypeMap[left] == posTypeMap[mid]:
				if posTypeMap[mid] == posTypeMap[right]:
					# どちらもOK
					pass
				else:
					# 後半は異なっている→後半を探す
					if same_found > 0 and random.randint(0,1):
						# 乱数要素を追加
						right = mid
						same_found -= 1
					else:
						left = mid
					continue
			else:
				# 前半は異なる
				if posTypeMap[mid] == posTypeMap[right]:
					# 前半だけ異なる→前半を探す
					if same_found > 0 and random.randint(0,1):
						# 乱数要素を追加
						left = mid
						same_found -= 1
					else:
						right = mid
					continue
				else:
					# どちらもOK
					pass
			# まだ調べていない方向を優先
			mid_l = int((mid - left)/2)
			mid_r = int((right - mid)/2)
			if not mid_l in posTypeMap:
				right = mid
				continue
			if not mid_r in posTypeMap:
				left = mid
				continue
			# 乱数で決める
			if random.randint(0, 1):
				left = mid
			else:
				right = mid

		# 終了処理
		self.PredPosList.sort()
		self.updatePredImageList()

# -------------------------
#	source image情報

	def fps(self):
		return int(self.cap.get( cv2.CAP_PROP_FPS ))

	def getPos(self):
		pos = self.cap.get( cv2.CAP_PROP_POS_FRAMES )
		return int(pos)

	def setPos(self, pos):
		self.cap.set( cv2.CAP_PROP_POS_FRAMES, pos )

	def getFrameCount(self):
		return int(self.cap.get( cv2.CAP_PROP_FRAME_COUNT ))

# -------------------------

	def getTypeByPosFromMarkList(self, pos):
		index = self.getPosListIndexByPosFromMarkList(pos)
		if index == -1:
			return -1
		return self.TypeList[self.PosList[index]]

	def getPosListIndexByPosFromMarkList(self, pos):
		result = None
		poslen = len(self.PosList)
		left = 0
		right = poslen
		if right == 0:
			return -1
		while left <= right:
			mid = (left + right) // 2
			if pos >= self.PosList[mid] and (mid == poslen-1 or pos < self.PosList[mid+1]):
				result = mid
				break
			if pos < self.PosList[mid]:
				right = mid - 1
			else:
				left = mid + 1
		if result == None:
			return -1
		else:
			return result

	def initLearn(self):
		if self.learnInitialized:
			return
		self.learnInitialized = True

		# Create the Estimator
		self.classifier = tf.estimator.Estimator(model_fn=cnn_model_fn, model_dir="./tmp/ai_gui_test")

		# Set up logging for predictions
		# Log the values in the "Softmax" tensor with label "probabilities"
		tensors_to_log = {"probabilities": "softmax_tensor"}
		self.logging_hook = tf.train.LoggingTensorHook(tensors=tensors_to_log, every_n_iter=50)

	# required: train_data, train_labels
	# ex. cnn_mnist.pyでは、
	# train_data: (55000, 784) = (55000, 28*28) : 55000は全データ数
	# train_labels: (55000,)
	def train(self, type):
		self.stepBack(None)
		ret, frame = self.cap.read()	# getPosが１つ進む
		if not ret:
			return

		# Build the input data
		if SHRINK_IMAGE:
			frame = cv2.resize(frame, (imageWidth, imageHeight))

		global pendingCount
		global train_data
		global train_labels

		data = frame.flatten().astype(np.float32)/255.0
		data_size = imageWidth * imageHeight * imageChannel

		if pendingCount == 0:
			train_data = np.empty((0,data_size))
			train_labels = np.empty(INPUT_SIZE, dtype=np.int32)

		train_data = np.append(train_data, np.array(data).reshape(1, data_size), axis=0).astype(np.float32)	# row方向に１つ追加

		print("train_data=", train_data.shape)
		print("train_labels=", train_labels.shape)
		print("pendingCount=", pendingCount)

		train_labels[pendingCount] = type

		pendingCount += 1

		if pendingCount < INPUT_SIZE:
			return

		pendingCount = 0

		# DEBUG: show the training data
		#fig, ax = plt.subplots(nrows=2, ncols=5, sharex=True, sharey=True)
		#ax = ax.flatten()
		#for i in range(10):
		#	img = train_data[i].reshape(28, 28) <- ここがうまくいかない
		#	ax[i].imshow(img, cmap='Greys')
		#	ax[0].set_xticks([])
		#	ax[0].set_yticks([])
		#	plt.tight_layout()
		#	plt.show()

		# Train the model
		self.train_input_fn = tf.estimator.inputs.numpy_input_fn(
		  x={"x": train_data},
		  y=train_labels,
		  batch_size=BATCH_SIZE,
		  #batch_size=100,
		  #batch_size=imageWidth*imageHeight*imageChannel,
		  num_epochs=None,
		  shuffle=True)
		self.classifier.train(
		  input_fn=self.train_input_fn,
		  steps=20000,
		  hooks=[self.logging_hook])

	def trainStart(self):
		self.initLearn()

		# 各typeのframe数の計算
		numlist = self.calcNumList()
		count = 0
		start_pos = 0
		end = False
		
		total_count = 0
		skip_data = 400

		start_time = time.time()

		for pos, numframe in zip(self.PosList, numlist):
			if end:
				break
			type = self.TypeList[pos]
			SKIP_FRAMES = 3
			for frame_index in range(numframe):
				if frame_index % SKIP_FRAMES != 0:
					continue

				if maxSec != 0 and pos + frame_index > maxSec * self.fps():
					end = True
					break

#				total_count += 1
#				if total_count < skip_data:
#					continue

				self.setPos(pos + frame_index)
				ret, frame = self.cap.read()	# getPosが１つ進む
				if not ret:
					break

				# Build the input data
				frame = cv2.resize(frame, (imageWidth, imageHeight))

				data = frame.flatten().astype(np.float32)/255.0
				data_size = imageWidth * imageHeight * imageChannel

				if count == 0:
					train_data = np.empty((0, data_size))
					train_labels = np.empty(INPUT_SIZE, dtype=np.int32)
					start_pos = pos + frame_index

				train_data = np.append(train_data, np.array(data).reshape(1, data_size), axis=0).astype(np.float32)	# row方向に１つ追加

				train_labels[count] = type
#				print("type=%d pos=%d" % (type, pos + frame_index))

#				print("count=", count)

				count += 1

				if count == INPUT_SIZE:
					start_tc = pos2str(start_pos)
#					print("train_data=", train_data.shape)
#					print("train_labels=", train_labels)
#					print("train_labels=", train_labels.shape)
					curpos = pos + frame_index
					str = "--- Training: %s %d - %d (%d step)" % (start_tc, start_pos, curpos, SKIP_FRAMES)
					logger.info(str)
					self.trainBatch(train_data, train_labels)
					count = 0

		# 残りの学習
		if count != 0:
			train_labels = np.resize(train_labels, (count, ))
			self.trainBatch(train_data, train_labels)

		elapsed_time = time.time() - start_time
		logger.info("-- End of training : (%d[sec])" % (elapsed_time))

	def trainBatch(self, train_data, train_labels):
		# DEBUG: show the training data
		#fig, ax = plt.subplots(nrows=2, ncols=5, sharex=True, sharey=True)
		#ax = ax.flatten()
		#for i in range(10):
		#	img = train_data[i].reshape(28, 28) <- ここがうまくいかない
		#	ax[i].imshow(img, cmap='Greys')
		#	ax[0].set_xticks([])
		#	ax[0].set_yticks([])
		#	plt.tight_layout()
		#	plt.show()

		# Train the model
		self.train_input_fn = tf.estimator.inputs.numpy_input_fn(
		  x={"x": train_data},
		  y=train_labels,
		  batch_size=BATCH_SIZE,
		  #batch_size=100,
		  #batch_size=imageWidth*imageHeight*imageChannel,
		  num_epochs=None,
		  shuffle=True)
		self.classifier.train(
		  input_fn=self.train_input_fn,
		  steps=20000,
		  hooks=[self.logging_hook])

	def evalProc(self, pos):
		self.initLearn()

		self.setPos(pos)
		ret, frame = self.cap.read()	# getPosが１つ進む
		if not ret:
			return

		# Build the input data
		frame = cv2.resize(frame, (imageWidth, imageHeight))

		data = frame.flatten().astype(np.float32)/255.0
		data_size = imageWidth * imageHeight * imageChannel

		eval_data = np.empty((0,data_size))
		eval_labels = np.empty(1, dtype=np.int32)

		eval_data = np.append(eval_data, np.array(data).reshape(1, data_size), axis=0).astype(np.float32)	# row方向に１つ追加

		type = self.getTypeByPosFromMarkList(pos)
		eval_labels[0] = type

		print("type=", type)
#		print("eval_data=", eval_data.shape)
#		print("eval_labels=", eval_labels.shape)

		self.eval_model(eval_data, eval_labels)

	def eval_model(self, eval_data, eval_labels):
		# Evaluate the model and print results
		eval_input_fn = tf.estimator.inputs.numpy_input_fn(
			x={"x": eval_data}, y=eval_labels, num_epochs=1, shuffle=False)
		eval_results = self.classifier.evaluate(input_fn=eval_input_fn)
		print(eval_results)	

	def predictProc(self, pos):
		self.initLearn()

		self.setPos(pos)
		ret, frame = self.cap.read()	# next frameに進む(read前に？)
		if not ret:
			return
		self.setPos(pos)

		# Build the input data
		frame = cv2.resize(frame, (imageWidth, imageHeight))

		data = frame.flatten().astype(np.float32)/255.0
		data_size = imageWidth * imageHeight * imageChannel

		eval_data = np.empty((0,data_size))
		eval_labels = np.empty(1, dtype=np.int32)

		eval_data = np.append(eval_data, np.array(data).reshape(1, data_size), axis=0).astype(np.float32)	# row方向に１つ追加

		type = self.getTypeByPosFromMarkList(pos)
		eval_labels[0] = type

		print("type=", type)
#		print("eval_data=", eval_data.shape)

		eval_results = self.predict_model(eval_data, eval_labels)

		#print(eval_results)
		for res in eval_results:
			print(res)
		pr = res['probabilities']
		print("1=%d 2=%d" % (int(pr[1]*100), int(pr[2]*100)))

		frame = cv2.cvtColor(frame, cv2.COLOR_BGR2RGB)


	# 1frameずつやると遅いかもしれない
	def predictFrame(self, pos):
		self.setPos(pos)
		ret, frame = self.cap.read()	# getPosが１つ進む
		if not ret:
			return 0

		# Build the input data
		frame = cv2.resize(frame, (imageWidth, imageHeight))

		data = frame.flatten().astype(np.float32)/255.0
		data_size = imageWidth * imageHeight * imageChannel

		eval_data = np.empty((0,data_size))
		eval_labels = np.empty(1, dtype=np.int32)

		eval_data = np.append(eval_data, np.array(data).reshape(1, data_size), axis=0).astype(np.float32)	# row方向に１つ追加

		type = self.getTypeByPosFromMarkList(pos)
		eval_labels[0] = type

#		print("type=", type)
#		print("eval_data=", eval_data.shape)

		eval_results = self.predict_model(eval_data, eval_labels)
		for res in eval_results:
			pass
		pr = res['probabilities']
		pr1 = int(pr[1]*1000)
		pr2 = int(pr[2]*1000)

		return 1 if pr1 > pr2 else 2

	def predict_model(self, eval_data, eval_labels):
		# Predict the model and print results
		eval_input_fn = tf.estimator.inputs.numpy_input_fn(
			x={"x": eval_data}, y=eval_labels, num_epochs=1, shuffle=False)
		eval_results = self.classifier.predict(input_fn=eval_input_fn)
		return eval_results

	def saveTrain(self):
		pass

	def calcNumList(self):
		numlist = []
		prev_pos = -1
		for pos in self.PosList:
			if prev_pos != -1:
				numlist.append(pos - prev_pos)
			prev_pos = pos
		if prev_pos == -1:
			prev_pos = 0
		numlist.append(self.getFrameCount() - prev_pos)
		return numlist


def cnn_model_fn(features, labels, mode):
  """Model function for CNN."""
  # Input Layer
  # Reshape X to 4-D tensor: [batch_size, width, height, channels]
  # MNIST images are 28x28 pixels, and have one color channel
  input_layer = tf.reshape(features["x"], [-1, imageWidth, imageHeight, imageChannel])

  # Convolutional Layer #1
  # Computes 32 features using a 5x5 filter with ReLU activation.
  # Padding is added to preserve width and height.
  # Input Tensor Shape: [batch_size, 28, 28, 1]
  # Output Tensor Shape: [batch_size, 28, 28, 32]
  conv1 = tf.layers.conv2d(
      inputs=input_layer,
      filters=32,
      kernel_size=[5, 5],
      padding="same",
      activation=tf.nn.relu)

  # Pooling Layer #1
  # First max pooling layer with a 2x2 filter and stride of 2
  # Input Tensor Shape: [batch_size, imageWidth, imageHeight, 32]
  # Output Tensor Shape: [batch_size, imageWidth/2, imageHeight/2, 32]
  pool1 = tf.layers.max_pooling2d(inputs=conv1, pool_size=[2, 2], strides=2)

  # Convolutional Layer #2
  # Computes 64 features using a 5x5 filter.
  # Padding is added to preserve width and height.
  # Input Tensor Shape: [batch_size, imageWidth/2, imageHeight/2, 32]
  # Output Tensor Shape: [batch_size, imageWidth/2, imageHeight/2, 64]
  conv2 = tf.layers.conv2d(
      inputs=pool1,
      filters=64,
      kernel_size=[5, 5],
      padding="same",
      activation=tf.nn.relu)

  # Pooling Layer #2
  # Second max pooling layer with a 2x2 filter and stride of 2
  # Input Tensor Shape: [batch_size, imageWidth/2, imageHeight/2, 64]
  # Output Tensor Shape: [batch_size, imageWidth/4, imageHeight/4, 64]
  pool2 = tf.layers.max_pooling2d(inputs=conv2, pool_size=[2, 2], strides=2)

  # Flatten tensor into a batch of vectors
  # Input Tensor Shape: [batch_size, imageWidth/4, imageHeight/4, 64]
  # Output Tensor Shape: [batch_size, imageWidth/4 * imageHeight/4 * 64]
  pool2_flat = tf.reshape(pool2, [-1, (imageWidth//4) * (imageHeight//4) * 64])

  # Dense Layer
  # Densely connected layer with 1024 neurons
  # Input Tensor Shape: [batch_size, imageWidth/4 * imageHeight/4 * 64]
  # Output Tensor Shape: [batch_size, 1024]
  dense = tf.layers.dense(inputs=pool2_flat, units=1024, activation=tf.nn.relu)

  # Add dropout operation; 0.6 probability that element will be kept
  dropout = tf.layers.dropout(
      inputs=dense, rate=0.4, training=mode == tf.estimator.ModeKeys.TRAIN)

  # Logits layer
  # Input Tensor Shape: [batch_size, 1024]
  # Output Tensor Shape: [batch_size, 10]
  logits = tf.layers.dense(inputs=dropout, units=10)

  predictions = {
      # Generate predictions (for PREDICT and EVAL mode)
      "classes": tf.argmax(input=logits, axis=1),
      # Add `softmax_tensor` to the graph. It is used for PREDICT and by the
      # `logging_hook`.
      "probabilities": tf.nn.softmax(logits, name="softmax_tensor")
  }
  if mode == tf.estimator.ModeKeys.PREDICT:
    return tf.estimator.EstimatorSpec(mode=mode, predictions=predictions)

  # Calculate Loss (for both TRAIN and EVAL modes)
  loss = tf.losses.sparse_softmax_cross_entropy(labels=labels, logits=logits)

  # Configure the Training Op (for TRAIN mode)
  if mode == tf.estimator.ModeKeys.TRAIN:
    optimizer = tf.train.GradientDescentOptimizer(learning_rate=0.001)
    train_op = optimizer.minimize(
        loss=loss,
        global_step=tf.train.get_global_step())
    return tf.estimator.EstimatorSpec(mode=mode, loss=loss, train_op=train_op)

  # Add evaluation metrics (for EVAL mode)
  eval_metric_ops = {
      "accuracy": tf.metrics.accuracy(
          labels=labels, predictions=predictions["classes"])}
  return tf.estimator.EstimatorSpec(
      mode=mode, loss=loss, eval_metric_ops=eval_metric_ops)

def pos2str(pos):
	return '%02d:%02d:%02d:%02d' % (int(pos/(3600*30)),int(pos/(60*30)%60), int(pos/30%60), int(pos%30))

import argparse

def main():
	parser = argparse.ArgumentParser(description='gui.pyで作ったデータを下に計算するよ！')
	parser.add_argument('mode', help='train | pred | eval')
	parser.add_argument('-n', '--target_number', default='1', help='target number')
	parser.add_argument('-d', '--duration', default=0, help='max duration(second)')
	args = parser.parse_args()

	global TargetFileNumber
	TargetFileNumber = str(args.target_number)
	global maxSec
	maxSec = int(args.duration)

	app = MyApp()
	if args.mode == 'train':
		app.trainStart()
	elif args.mode == 'pred':
		app.predictAll()
	else:
		parser.usage()

if __name__ == '__main__':
	main()

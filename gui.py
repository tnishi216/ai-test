
import wx
import cv2
import numpy as np
import time
import random
from socket import gethostname

# C:/temp/test1.mp4
# 5:28 PG IN
# 59:28 CM IN
# 2:59:28 PG IN

# for learning
import tensorflow as tf

# for debug
import matplotlib.pyplot as plt

# Internal modules
from marklist import MarkList
import dialogs

## Configuration ##

TargetFileNumber = '4'

TargetMp4File = "test"+TargetFileNumber+".mp4"
TargetMarkListFile = "marklist"+TargetFileNumber+".txt"

TargetImageFile = None

#tf.logging.set_verbosity(tf.logging.WARN)	# Ver.1.13以降？

import os
os.environ['TF_CPP_MIN_LOG_LEVEL'] = '2'

LOG_ENABLED = False

# 学習用画像のサイズ
imageWidth = 56
imageHeight = 56

# 元画像のサイズ
orgWidth = imageWidth
orgHeight = imageHeight

imageChannel = 3
SHRINK_IMAGE = True
BATCH_SIZE = 100
INPUT_SIZE = 200

imageViewWidth = 278
imageViewHeight = 446

# Thumbnail size
tbWidth = 139
tbHeight = 73

# working values
pendingCount = 0

# Ref.: https://www.tensorflow.org/tutorials/estimators/cnn
# org github: https://github.com/tensorflow/tensorflow/blob/master/tensorflow/examples/tutorials/layers/cnn_mnist.py

# 参考：https://www.pyimagesearch.com/2017/02/06/faster-video-file-fps-with-cv2-videocapture-and-opencv/ (decode threading)

class MyApp(wx.Frame):

	def __init__(self, *args, **kw):
		super(MyApp, self).__init__(*args, **kw)
		self.learnInitialized = False
		self.bitmapImage = None

		global TargetMp4File
		global TargetMarkListFile
		TargetMp4File = "test"+TargetFileNumber+".mp4"
		TargetMarkListFile = "marklist"+TargetFileNumber+".txt"

		self.PosList = []
		self.TypeList = {}

		self.PredPosList = []
		self.PredTypeList = {}

		# File operation
		self.edited = False
		self.filename = TargetMarkListFile

		self.init_ui()

	def init_ui(self):

		self.SetSize(800, 600)

		# Main Menu
		menu = wx.MenuBar()
		# File menu
		file = wx.Menu()
		self.Bind(wx.EVT_MENU, self.onFileNew, file.Append(-1, "&New"))
		self.Bind(wx.EVT_MENU, self.onFileLoad, file.Append(2, "&Load"))
		self.Bind(wx.EVT_MENU, self.onFileSave, file.Append(3, "&Save"))
		self.Bind(wx.EVT_MENU, self.onFileSaveAs, file.Append(4, "Save &As"))
		file.AppendSeparator()
		self.Bind(wx.EVT_MENU, self.onFileExit, file.Append(5, "E&xit"))
		# Edit menu
		edit = wx.Menu()
		self.Bind(wx.EVT_MENU, self.onEditUndo, file.Append(-1, "&Undo mark"))
		# Tool menu
		
		menu.Append(file, "&File")
		self.SetMenuBar(menu)

		# root panel
		pnlRoot = wx.Panel(self, wx.ID_ANY)

		# panelの生成
		pnlLeft = wx.Panel(pnlRoot, wx.ID_ANY)

		self.pnlImage = wx.Panel(pnlLeft, -1, pos=(10, 10), size=(imageViewWidth, imageViewHeight))

		pnlStatus = wx.Panel(pnlLeft, -1)

		panel = wx.Panel(pnlLeft, -1)

		# Build status panel
		layout = wx.BoxSizer(wx.HORIZONTAL)
		self.stxStatus = wx.StaticText(pnlStatus, -1, '00:00:00:00')
		layout.Add(self.stxStatus)
		self.stxInfo = wx.StaticText(pnlStatus, -1, '')
		layout.Add(self.stxInfo)
		pnlStatus.SetSizer(layout)

		layout = wx.BoxSizer(wx.HORIZONTAL)

		# buttonの生成
		#self.btnLoad = self.createButton(panel, 'Load', layout, self.load)
		self.btnStepBack = wx.Button(panel, wx.ID_ANY, 'Back')
		self.btnStepNext = wx.Button(panel, wx.ID_ANY, 'Next')
		self.btnBackSec = wx.Button(panel, wx.ID_ANY, 'BackSec')
		self.btnNextSec = wx.Button(panel, wx.ID_ANY, 'NextSec')

		# layoutの生成 for buttons
		layout.Add( self.btnStepBack )
		layout.Add( self.btnStepNext )
		layout.Add( self.btnBackSec )
		layout.Add( self.btnNextSec )

		# event handlerのbind
		self.btnStepBack.Bind(wx.EVT_BUTTON, self.stepBack)
		#self.btnStepBack.Bind(wx.EVT_LEFT_DOWN, self.stepBackLeftDown)
		#self.btnStepBack.Bind(wx.EVT_LEFT_UP, self.stepBackLeftUp)
		self.btnStepNext.Bind(wx.EVT_BUTTON, self.stepNext)
		self.btnBackSec.Bind(wx.EVT_BUTTON, self.backSec)
		self.btnNextSec.Bind(wx.EVT_BUTTON, self.nextSec)

		# layout設定
		panel.SetSizer(layout)

		# 二段目のbutton creation
		layout = wx.BoxSizer(wx.HORIZONTAL)
		panel2 = wx.Panel(pnlLeft, -1)
		self.btnBack15Sec = wx.Button(panel2, wx.ID_ANY, 'Back15Sec')
		self.btnNext15Sec = wx.Button(panel2, wx.ID_ANY, 'Next15Sec')
		self.btnPlayFF = wx.Button(panel2, wx.ID_ANY, 'PlayFF')
		layout.Add( self.btnBack15Sec )
		layout.Add( self.btnNext15Sec )
		layout.Add( self.btnPlayFF )
		self.btnBack15Sec.Bind(wx.EVT_BUTTON, self.back15Sec)
		self.btnNext15Sec.Bind(wx.EVT_BUTTON, self.next15Sec)
		self.btnPlayFF.Bind(wx.EVT_BUTTON, self.playFF)
		panel2.SetSizer(layout)

		# 三段目のbutton creation
		layout = wx.BoxSizer(wx.HORIZONTAL)
		panel3 = wx.Panel(pnlLeft, -1)
		self.createButton(panel3, 'Type &1', layout, self.type1)
		self.createButton(panel3, 'Type &2', layout, self.type2)
		self.createButton(panel3, 'Type &0', layout, self.type0)
		panel3.SetSizer(layout)

		layout = wx.BoxSizer(wx.HORIZONTAL)
		panel4 = wx.Panel(pnlLeft, -1)
		self.createButton(panel4, '&Learn', layout, self.trainStartEvent)
		self.createButton(panel4, '&Predict', layout, self.predict)
		self.createButton(panel4, 'Predict &All', layout, self.predictAll)
		panel4.SetSizer(layout)

		layout = wx.BoxSizer(wx.HORIZONTAL)
		panel5 = wx.Panel(pnlLeft, -1)
		self.stxProgress = wx.StaticText(panel5, -1, 'xxxxxxxxxxxxxxxx')
		layout.Add(self.stxProgress)
		panel5.SetSizer(layout)

		# edit側のlayout
		layout = wx.BoxSizer(wx.VERTICAL)
		layout.Add(pnlStatus)
		layout.Add(panel)
		layout.Add(panel2)
		layout.Add(panel3)
		layout.Add(panel4)
		layout.Add(panel5)
		layout.Add(self.pnlImage)
		pnlLeft.SetSizer(layout)

		# 右側のmark image list
		pnlRight = wx.Panel(pnlRoot, wx.ID_ANY)

		layout = wx.BoxSizer(wx.VERTICAL)
		self.lcMarkImageList = wx.ListCtrl(pnlRight, wx.ID_ANY, size=(tbWidth+60,tbHeight*8), style=wx.LC_ICON)
		self.lcMarkImageList.Bind(wx.EVT_LIST_ITEM_SELECTED, self.evMarkImageList)
		layout.Add(self.lcMarkImageList)

		pnlRight.SetSizer(layout)

		# さらに右側のpredict結果一覧
		pnlPred = wx.Panel(pnlRoot, wx.ID_ANY)

		layout = wx.BoxSizer(wx.VERTICAL)
		self.lcPredImageList = wx.ListCtrl(pnlPred, wx.ID_ANY, size=(tbWidth+60,tbHeight*8), style=wx.LC_ICON)
		self.lcPredImageList.Bind(wx.EVT_LIST_ITEM_SELECTED, self.evPredImageList)
		layout.Add(self.lcPredImageList)

		pnlPred.SetSizer(layout)

		# 全体のlayout
		layout = wx.BoxSizer(wx.HORIZONTAL)
		layout.Add(pnlLeft)
		layout.Add(pnlRight)
		layout.Add(pnlPred)
		pnlRoot.SetSizer(layout)

		layout.Fit(pnlRoot)

		"""
		# easy version
		self.btn = wx.Button(panel, -1, 'Load', pos=(2,5))
		self.btn.Bind(wx.EVT_BUTTON, self.load)

		self.btnStepBack = wx.Button(panel, -1, 'Back', pos=(100,5))
		self.btnStepBack.Bind(wx.EVT_BUTTON, self.stepBack)

		self.btnStepNext = wx.Button(panel, -1, 'Next', pos=(200,5))
		self.btnStepNext.Bind(wx.EVT_BUTTON, self.stepNext)
		"""

		self.Show()

		self.load(None)

		self.loadMarkList()

#		self.testTrain()

	def onFileNew(self, event):
		if self.closeQuery() != False:
			return
		self.filename = ''

	def onFileLoad(self, event):
		self.filename = TargetMarkListFile
		self.loadMarkList()

	def onFileSave(self, event):
		self.saveMarkList()
		self.saveTrain()
	
	def onFileSaveAs(self, event):
		pass

	def onFileExit(self, event):
		if self.closeQuery() == False:
			return
		self.Close()

	def closeQuery(self):
		if self.edited:
			dlg = wx.MessageBox(None, "Mark List is not saved yet. Are you sure to exit?", "Exit", wx.YES_NO_CANCEL | wx.ICON_QUESTION)
			result = dlg.ShowModal()
			if result == wx.YES:
				self.onFileSaveAs()
			elif result == wx.NO:
				return True
			return False
		return True

	def onEditUndo(self, event):
		pass

	def createButton(self, panel, title, layout, handler):
		button = wx.Button(panel, -1, title)
		layout.Add( button )
		button.Bind(wx.EVT_BUTTON, handler)
		return button

	def load(self, event):
		if gethostname() == 'nishikawat-PC':
			FileName = "R:/temp/prs59i.mp4"		# 480x270 x3
		else:
			#FileName = "Q:/cap/test1.mp4"		# 268x148 x3
			FileName = "./image/"+TargetMp4File	# 268x148 x3
			# FileName = "Q:/cap/amarecco20180415-173633[000].avi"
		if TargetImageFile is not None:
			FileName = TargetImageFile
		if not os.path.exists(FileName):
			print("No file: ", FileName)
			exit()
		self.cap = cv2.VideoCapture(FileName)
		global imageWidth, imageHeight
		global orgWidth, orgHeight
		orgWidth = int(self.cap.get(cv2.CAP_PROP_FRAME_WIDTH))
		orgHeight = int(self.cap.get(cv2.CAP_PROP_FRAME_HEIGHT))
		if not SHRINK_IMAGE:
			imageWidth = orgWidth
			imageHeight = orgHeight
		
		# Show Info
		text = "%d x %d %dfps" % (orgWidth, orgHeight, self.fps())
		self.stxInfo.SetLabel(text)
		
		# Resize controls
		global tbWidth, tbHeight
		tbWidth = orgWidth / 2
		tbHeight = orgHeight / 2
		self.pnlImage.SetSize(orgWidth, orgHeight);
		self.lcMarkImageList.SetSize(tbWidth+60, tbHeight*8)
		
		self.setFrame()

	def loadMarkList(self):
		if self.filename != '':
			marklist = MarkList()
			marklist.load(self.filename)
			self.PosList = marklist.frames
			self.TypeList = marklist.types
		self.updateMarkList()

	def saveMarkList(self):
		if self.filename == '':
			return
		marklist = MarkList()
		marklist.frames = self.PosList
		marklist.types = self.TypeList
		marklist.save(self.filename)

	def evMarkImageList(self, event):
		select_index = self.lcMarkImageList.GetFirstSelected()
		pos = self.PosList[select_index]
		self.setPos( pos )
		self.setFrame()

	def updateMarkList(self):
		imgList = wx.ImageList(tbWidth, tbHeight)

		for pos in self.PosList:
			bmp = self.getBitmapFrame(pos)
			img = bmp.ConvertToImage()
			img = img.Scale(tbWidth, tbHeight)
			imgList.Add( img.ConvertToBitmap() )
		self.lcMarkImageList.AssignImageList(imgList, wx.IMAGE_LIST_NORMAL)

		self.lcMarkImageList.ClearAll()

		i = 0
		for pos in self.PosList:
			type = self.TypeList[pos]
			title = pos2str(pos) + " " + str(type)
			self.lcMarkImageList.InsertItem(i, title, i)
			i += 1

	# テスト用の自動学習
	def testTrain(self):
		self.learn(1)
		

	def stepBack(self, event):
		self.setPos( self.getPos() - 2 )
		self.setFrame()

	def stepBackLeftDown(self, event):
		super
		print("Left Down")

	def stepBackLeftUp(self, event):
		super
		print("Left Up")

	def stepNext(self, event):
		self.setFrame()

	def backSec(self, event):
		pos = self.getPos()
		if pos == 0:
			return
		pos = pos - self.fps() - 1
		if pos < 0:
			pos = 0
		self.setPos( pos )
		self.setFrame()

	def nextSec(self, event):
		pos = self.getPos()
		if pos == self.getFrameCount()-1:
			return
		pos = pos + self.fps() - 1
		if pos >= self.getFrameCount():
			pos = self.getFrameCount() - 1
		self.setPos( pos )
		self.setFrame()

	def back15Sec(self, event):
		pos = self.getPos()
		if pos == 0:
			return
		pos = pos - self.fps()*15 - 1
		if pos < 0:
			pos = 0
		self.setPos( pos )
		self.setFrame()

	def next15Sec(self, event):
		pos = self.getPos()
		if pos == self.getFrameCount()-1:
			return
		pos = self.getPos() + self.fps()*15 - 1
		if pos >= self.getFrameCount():
			pos = self.getFrameCount()-1
		self.setPos( pos )
		self.setFrame()

	def playFF(self, event):
		pass

	def type1(self, evet):
		self.setType(1)

	def type2(self, evet):
		self.setType(2)

	def type0(self, evet):
		self.setType(0)

	def setType(self, type):
		pos = self.getPos() - 1		# なぜか -1 すると合う
		self.PosList.append(pos)
		self.TypeList[pos] = type
		self.updateMarkList()

	def learn1(self, event):
		self.learn(1)

	def learn2(self, event):
		self.learn(2)

	def learn3(self, event):
		self.learn(3)

	def learn(self, type):
		self.initLearn()
		self.train(type)

	def predict(self, event):
		#self.evalProc(self.getPos())
		self.predictProc(self.getPos())

	# 全frameから変化点を一覧する
	def predictAll(self, event):
		maxSec = dialogs.inputMaxPredictSec()
		if maxSec < 0:
			return

		SKIP_FRAMES = 15
	
		# Initialize
		self.PredPosList = []
		self.PredTypeList = {}
		self.PredPosList.clear()

		count = 0

		start_time = time.time()

		self.initLearn()
		tf.logging.set_verbosity(tf.logging.WARN)
		prev_type = 0
		start_pos = 0
		for pos in range(0, self.getFrameCount(), SKIP_FRAMES):
			if maxSec != 0 and pos >= maxSec * self.fps():
				break

			type = self.predictFrame(pos)
			if type == 0:
				break

#			print("pos=%d type=%d" % (pos, type))

			if prev_type != type:
				if prev_type != 0:
					# 先頭以外
					for i in (start_pos+1, pos):
						# 正確な場所を調べる
#						print("i=",i)
						t = self.predictFrame(i)
						if t == 0:
							print("??? BUG???")
							break
						if t == type:
							# 正確な位置
							pos = i
							break

#				print("---------------------------------------")
				percent = int(pos * 100 / self.getFrameCount())
				print("-- %s(%d/%d %d%%) %d" % (pos2str(pos), pos, self.getFrameCount(), percent, type))
				start_pos = pos
				prev_type = type
#				input('Next to push any key')

				self.PredPosList.append(start_pos)
				self.PredTypeList[pos] = type
				count += 1
#				if count == 2:
#					break

		elapsed_time = time.time() - start_time

		self.updatePredImageList()

		print("-- End of predictAll (%d[sec])" % (elapsed_time))

	# 半日かけて意味のないものを作ってしまった・・・
	def predictAllBS(self, event):
		maxScene = dialogs.inputMaxScene()
		if maxScene < 0:
			return

		self.PredPosList = []
		self.PredTypeList = {}
		self.PredPosList.clear()

		posTypeMap = {}

		found = 0
		not_found = 0	# 連続して見つからなかった回数
		old_found = 0	# 
		same_found = 0	# 連続して同じscene change frameが見つかった回数
		MaxSameFound = 10
		prev_found_pos = -1

		self.initLearn()
		tf.logging.set_verbosity(tf.logging.WARN)
		left = 0
		right = self.getFrameCount() - 1
		while True:
#			print("left=%d right=%d" % (left, right))
			if right <= left + 1:
				if posTypeMap[left] != posTypeMap[right]:
					# found
					pos = right
					if not pos in self.PredTypeList:
						# new found
						found += 1
						print("-- new found pos=%d type=%d scene=%d" % (pos, posTypeMap[pos], found))
						self.PredPosList.append(pos)
						self.PredTypeList[pos] = posTypeMap[pos]
						if found >= maxScene:
							# すべて見つけた
							#TODO: self.PredTypeListを見て、不連続なところだけやり直す
							break
						old_found = 0
					else:
						print("-- old found pos=%d type=%d same_found=%d" % (pos, posTypeMap[pos], same_found))
						old_found += 1
						if prev_found_pos == pos:
							same_found += 1
							if same_found >= MaxSameFound:
								print("!! too many same found")
								break
						else:
							same_found = 0
					prev_found_pos = pos
					not_found = 0
				else:
					not_found += 1
					same_found = 0
					print("-- Not found: left=%d right=%d" % (left, right))
				left = 0
				right = self.getFrameCount() - 1
				continue
			mid = int((right + left) / 2)
			if not left in posTypeMap:
				posTypeMap[left] = self.predictFrame(left)
			if not right in posTypeMap:
				posTypeMap[right] = self.predictFrame(right)
			if not mid in posTypeMap:
				posTypeMap[mid] = self.predictFrame(mid)
			# 異なるtypeのほうへ進める
			if posTypeMap[left] == posTypeMap[mid]:
				if posTypeMap[mid] == posTypeMap[right]:
					# どちらもOK
					pass
				else:
					# 後半は異なっている→後半を探す
					if same_found > 0 and random.randint(0,1):
						# 乱数要素を追加
						right = mid
						same_found -= 1
					else:
						left = mid
					continue
			else:
				# 前半は異なる
				if posTypeMap[mid] == posTypeMap[right]:
					# 前半だけ異なる→前半を探す
					if same_found > 0 and random.randint(0,1):
						# 乱数要素を追加
						left = mid
						same_found -= 1
					else:
						right = mid
					continue
				else:
					# どちらもOK
					pass
			# まだ調べていない方向を優先
			mid_l = int((mid - left)/2)
			mid_r = int((right - mid)/2)
			if not mid_l in posTypeMap:
				right = mid
				continue
			if not mid_r in posTypeMap:
				left = mid
				continue
			# 乱数で決める
			if random.randint(0, 1):
				left = mid
			else:
				right = mid

		# 終了処理
		self.PredPosList.sort()
		self.updatePredImageList()

	def setFrame(self):
		pos = self.getPos()
		self.setPos(pos)
		ret, frame = self.cap.read()	# getPosが１つ進む
		if not ret:
			return
		self.setStatusLabel(int(pos))
		frame = cv2.cvtColor(frame, cv2.COLOR_BGR2RGB)
		img = wx.Bitmap.FromBuffer(frame.shape[1], frame.shape[0], frame)
		if self.bitmapImage is None:
			self.bitmapImage = wx.StaticBitmap(self.pnlImage, -1, img, pos=(0,0), size=(orgWidth, orgHeight))
		else:
			self.bitmapImage.SetBitmap( img )

	def getBitmapFrame(self, pos):
		orgpos = self.getPos()
		self.setPos(pos)
		ret, frame = self.cap.read()	# getPosが１つ進む
		if not ret:
			return
		frame = cv2.cvtColor(frame, cv2.COLOR_BGR2RGB)
		img = wx.Bitmap.FromBuffer(frame.shape[1], frame.shape[0], frame)
		self.setPos(orgpos)
		return img

	def setStatusLabel(self, pos):
		str = pos2str(pos)
		self.stxStatus.SetLabel(str)

	def updatePredImageList(self):
		imgList = wx.ImageList(tbWidth, tbHeight)

		for pos in self.PredPosList:
			bmp = self.getBitmapFrame(pos)
			img = bmp.ConvertToImage()
			img = img.Scale(tbWidth, tbHeight)
			imgList.Add( img.ConvertToBitmap() )
		self.lcPredImageList.AssignImageList(imgList, wx.IMAGE_LIST_NORMAL)

		self.lcPredImageList.ClearAll()

		i = 0
		for pos in self.PredPosList:
			type = self.PredTypeList[pos]
			title = pos2str(pos) + " " + str(type)
			self.lcPredImageList.InsertItem(i, title, i)
			i += 1

	def evPredImageList(self, event):
		select_index = self.lcPredImageList.GetFirstSelected()
		pos = self.PredPosList[select_index]
		self.setPos( pos )
		self.setFrame()

# -------------------------
#	source image情報

	def fps(self):
		return int(self.cap.get( cv2.CAP_PROP_FPS ))

	def getPos(self):
		pos = self.cap.get( cv2.CAP_PROP_POS_FRAMES )
		return int(pos)

	def setPos(self, pos):
		self.cap.set( cv2.CAP_PROP_POS_FRAMES, pos )

	def getFrameCount(self):
		return int(self.cap.get( cv2.CAP_PROP_FRAME_COUNT ))

# -------------------------

	def getTypeByPosFromMarkList(self, pos):
		index = self.getPosListIndexByPosFromMarkList(pos)
		if index == -1:
			return -1
		return self.TypeList[self.PosList[index]]

	def getPosListIndexByPosFromMarkList(self, pos):
		result = None
		poslen = len(self.PosList)
		left = 0
		right = poslen
		if right == 0:
			return -1
		while left <= right:
			mid = (left + right) // 2
			if pos >= self.PosList[mid] and (mid == poslen-1 or pos < self.PosList[mid+1]):
				result = mid
				break
			if pos < self.PosList[mid]:
				right = mid - 1
			else:
				left = mid + 1
		if result == None:
			return -1
		else:
			return result

	def initLearn(self):
		if self.learnInitialized:
			return
		self.learnInitialized = True

		# Create the Estimator
		self.classifier = tf.estimator.Estimator(model_fn=cnn_model_fn, model_dir="/tmp/ai_gui_test")

		# Set up logging for predictions
		# Log the values in the "Softmax" tensor with label "probabilities"
		tensors_to_log = {"probabilities": "softmax_tensor"}
		self.logging_hook = tf.train.LoggingTensorHook(tensors=tensors_to_log, every_n_iter=50)

	# required: train_data, train_labels
	# ex. cnn_mnist.pyでは、
	# train_data: (55000, 784) = (55000, 28*28) : 55000は全データ数
	# train_labels: (55000,)
	def train(self, type):
		self.stepBack(None)
		ret, frame = self.cap.read()	# getPosが１つ進む
		if not ret:
			return

		# Build the input data
		if SHRINK_IMAGE:
			frame = cv2.resize(frame, (imageWidth, imageHeight))

		global pendingCount
		global train_data
		global train_labels

		data = frame.flatten().astype(np.float32)/255.0
		data_size = imageWidth * imageHeight * imageChannel

		if pendingCount == 0:
			train_data = np.empty((0,data_size))
			train_labels = np.empty(INPUT_SIZE, dtype=np.int32)

		train_data = np.append(train_data, np.array(data).reshape(1, data_size), axis=0).astype(np.float32)	# row方向に１つ追加

		print("train_data=", train_data.shape)
		print("train_labels=", train_labels.shape)
		print("pendingCount=", pendingCount)

		train_labels[pendingCount] = type

		pendingCount += 1

		if pendingCount < INPUT_SIZE:
			return

		pendingCount = 0

		# DEBUG: show the training data
		#fig, ax = plt.subplots(nrows=2, ncols=5, sharex=True, sharey=True)
		#ax = ax.flatten()
		#for i in range(10):
		#	img = train_data[i].reshape(28, 28) <- ここがうまくいかない
		#	ax[i].imshow(img, cmap='Greys')
		#	ax[0].set_xticks([])
		#	ax[0].set_yticks([])
		#	plt.tight_layout()
		#	plt.show()

		# Train the model
		self.train_input_fn = tf.estimator.inputs.numpy_input_fn(
		  x={"x": train_data},
		  y=train_labels,
		  batch_size=BATCH_SIZE,
		  #batch_size=100,
		  #batch_size=imageWidth*imageHeight*imageChannel,
		  num_epochs=None,
		  shuffle=True)
		self.classifier.train(
		  input_fn=self.train_input_fn,
		  steps=20000,
		  hooks=[self.logging_hook])

	# Thumbnail listに合わせて一括でtraining
	def trainStartEvent(self, event):
		self.trainStart()

	def trainStart(self):
		maxSec = dialogs.inputMaxTrainSec()
		if maxSec < 0:
			return

		self.initLearn()

		# 各typeのframe数の計算
		numlist = self.calcNumList()
		count = 0
		start_pos = 0
		end = False
		
		total_count = 0
		skip_data = 400

		start_time = time.time()

		for pos, numframe in zip(self.PosList, numlist):
			if end:
				break
			type = self.TypeList[pos]
			SKIP_FRAMES = 3
			for frame_index in range(numframe):
				if frame_index % SKIP_FRAMES != 0:
					continue

				if maxSec != 0 and pos + frame_index > maxSec * self.fps():
					end = True
					break

#				total_count += 1
#				if total_count < skip_data:
#					continue

				self.setPos(pos + frame_index)
				ret, frame = self.cap.read()	# getPosが１つ進む
				if not ret:
					break

				# Build the input data
				frame = cv2.resize(frame, (imageWidth, imageHeight))

				data = frame.flatten().astype(np.float32)/255.0
				data_size = imageWidth * imageHeight * imageChannel

				if count == 0:
					train_data = np.empty((0, data_size))
					train_labels = np.empty(INPUT_SIZE, dtype=np.int32)
					start_pos = pos + frame_index
					print("train_data=", train_data)

				train_data = np.append(train_data, np.array(data).reshape(1, data_size), axis=0).astype(np.float32)	# row方向に１つ追加

				train_labels[count] = type
#				print("type=%d pos=%d" % (type, pos + frame_index))

#				print("count=", count)

				count += 1

				if count == INPUT_SIZE:
					start_tc = pos2str(start_pos)
#					print("train_data=", train_data.shape)
#					print("train_labels=", train_labels)
#					print("train_labels=", train_labels.shape)
					curpos = pos + frame_index
					str = "--- Training: %s %d - %d (%d step)" % (start_tc, start_pos, curpos, SKIP_FRAMES)
					print(str)
					self.stxProgress.SetLabel(str)
					self.trainBatch(train_data, train_labels)
					count = 0

		# 残りの学習 (エラーが発生するのでとりあえずコメントアウト)
		if count != 0:
			train_labels = np.resize(train_labels, (count, ))
			self.trainBatch(train_data, train_labels)

		elapsed_time = time.time() - start_time
		print(f"--- End of training : {elapsed_time}[sec]")

	def trainBatch(self, train_data, train_labels):
		# DEBUG: show the training data
		#fig, ax = plt.subplots(nrows=2, ncols=5, sharex=True, sharey=True)
		#ax = ax.flatten()
		#for i in range(10):
		#	img = train_data[i].reshape(28, 28) <- ここがうまくいかない
		#	ax[i].imshow(img, cmap='Greys')
		#	ax[0].set_xticks([])
		#	ax[0].set_yticks([])
		#	plt.tight_layout()
		#	plt.show()

		# Train the model
		self.train_input_fn = tf.estimator.inputs.numpy_input_fn(
		  x={"x": train_data},
		  y=train_labels,
		  batch_size=BATCH_SIZE,
		  #batch_size=100,
		  #batch_size=imageWidth*imageHeight*imageChannel,
		  num_epochs=None,
		  shuffle=True)
		self.classifier.train(
		  input_fn=self.train_input_fn,
		  steps=20000,
		  hooks=[self.logging_hook])

	def evalProc(self, pos):
		self.initLearn()

		self.setPos(pos)
		ret, frame = self.cap.read()	# getPosが１つ進む
		if not ret:
			return

		# Build the input data
		frame = cv2.resize(frame, (imageWidth, imageHeight))

		data = frame.flatten().astype(np.float32)/255.0
		data_size = imageWidth * imageHeight * imageChannel

		eval_data = np.empty((0,data_size))
		eval_labels = np.empty(1, dtype=np.int32)

		eval_data = np.append(eval_data, np.array(data).reshape(1, data_size), axis=0).astype(np.float32)	# row方向に１つ追加

		type = self.getTypeByPosFromMarkList(pos)
		eval_labels[0] = type

		print("type=", type)
#		print("eval_data=", eval_data.shape)
#		print("eval_labels=", eval_labels.shape)

		self.eval_model(eval_data, eval_labels)

	def eval_model(self, eval_data, eval_labels):
		# Evaluate the model and print results
		eval_input_fn = tf.estimator.inputs.numpy_input_fn(
			x={"x": eval_data}, y=eval_labels, num_epochs=1, shuffle=False)
		eval_results = self.classifier.evaluate(input_fn=eval_input_fn)
		print(eval_results)	

	def predictProc(self, pos):
		self.initLearn()

		self.setPos(pos)
		ret, frame = self.cap.read()	# next frameに進む(read前に？)
		if not ret:
			return
		self.setPos(pos)

		# Build the input data
		frame = cv2.resize(frame, (imageWidth, imageHeight))

		data = frame.flatten().astype(np.float32)/255.0
		data_size = imageWidth * imageHeight * imageChannel

		eval_data = np.empty((0,data_size))
		eval_labels = np.empty(1, dtype=np.int32)

		eval_data = np.append(eval_data, np.array(data).reshape(1, data_size), axis=0).astype(np.float32)	# row方向に１つ追加

		type = self.getTypeByPosFromMarkList(pos)
		eval_labels[0] = type

		print("type=", type)
#		print("eval_data=", eval_data.shape)

		eval_results = self.predict_model(eval_data, eval_labels)

		#print(eval_results)
		for res in eval_results:
			print(res)
		pr = res['probabilities']
		print("1=%d 2=%d" % (int(pr[1]*100), int(pr[2]*100)))

		frame = cv2.cvtColor(frame, cv2.COLOR_BGR2RGB)
		img = wx.Bitmap.FromBuffer(frame.shape[1], frame.shape[0], frame)
		if self.bitmapImage is None:
			self.bitmapImage = wx.StaticBitmap(self.pnlImage, -1, img, pos=(0,0), size=(orgWidth, orgHeight))


	# 1frameずつやると遅いかもしれない
	def predictFrame(self, pos):
		self.setPos(pos)
		ret, frame = self.cap.read()	# getPosが１つ進む
		if not ret:
			return 0

		# Build the input data
		frame = cv2.resize(frame, (imageWidth, imageHeight))

		data = frame.flatten().astype(np.float32)/255.0
		data_size = imageWidth * imageHeight * imageChannel

		eval_data = np.empty((0,data_size))
		eval_labels = np.empty(1, dtype=np.int32)

		eval_data = np.append(eval_data, np.array(data).reshape(1, data_size), axis=0).astype(np.float32)	# row方向に１つ追加

		type = self.getTypeByPosFromMarkList(pos)
		eval_labels[0] = type

#		print("type=", type)
#		print("eval_data=", eval_data.shape)

		eval_results = self.predict_model(eval_data, eval_labels)
		for res in eval_results:
			pass
		pr = res['probabilities']
		pr1 = int(pr[1]*1000)
		pr2 = int(pr[2]*1000)

		return 1 if pr1 > pr2 else 2

	def predict_model(self, eval_data, eval_labels):
		# Predict the model and print results
		eval_input_fn = tf.estimator.inputs.numpy_input_fn(
			x={"x": eval_data}, y=eval_labels, num_epochs=1, shuffle=False)
		eval_results = self.classifier.predict(input_fn=eval_input_fn)
		return eval_results

	def saveTrain(self):
		pass

	def calcNumList(self):
		numlist = []
		prev_pos = -1
		for pos in self.PosList:
			if prev_pos != -1:
				numlist.append(pos - prev_pos)
			prev_pos = pos
		if prev_pos == -1:
			prev_pos = 0
		numlist.append(self.getFrameCount() - prev_pos)
		return numlist


def cnn_model_fn(features, labels, mode):
  """Model function for CNN."""
  # Input Layer
  # Reshape X to 4-D tensor: [batch_size, width, height, channels]
  # MNIST images are 28x28 pixels, and have one color channel
  input_layer = tf.reshape(features["x"], [-1, imageWidth, imageHeight, imageChannel])

  # Convolutional Layer #1
  # Computes 32 features using a 5x5 filter with ReLU activation.
  # Padding is added to preserve width and height.
  # Input Tensor Shape: [batch_size, 28, 28, 1]
  # Output Tensor Shape: [batch_size, 28, 28, 32]
  conv1 = tf.layers.conv2d(
      inputs=input_layer,
      filters=32,
      kernel_size=[5, 5],
      padding="same",
      activation=tf.nn.relu)

  # Pooling Layer #1
  # First max pooling layer with a 2x2 filter and stride of 2
  # Input Tensor Shape: [batch_size, imageWidth, imageHeight, 32]
  # Output Tensor Shape: [batch_size, imageWidth/2, imageHeight/2, 32]
  pool1 = tf.layers.max_pooling2d(inputs=conv1, pool_size=[2, 2], strides=2)

  # Convolutional Layer #2
  # Computes 64 features using a 5x5 filter.
  # Padding is added to preserve width and height.
  # Input Tensor Shape: [batch_size, imageWidth/2, imageHeight/2, 32]
  # Output Tensor Shape: [batch_size, imageWidth/2, imageHeight/2, 64]
  conv2 = tf.layers.conv2d(
      inputs=pool1,
      filters=64,
      kernel_size=[5, 5],
      padding="same",
      activation=tf.nn.relu)

  # Pooling Layer #2
  # Second max pooling layer with a 2x2 filter and stride of 2
  # Input Tensor Shape: [batch_size, imageWidth/2, imageHeight/2, 64]
  # Output Tensor Shape: [batch_size, imageWidth/4, imageHeight/4, 64]
  pool2 = tf.layers.max_pooling2d(inputs=conv2, pool_size=[2, 2], strides=2)

  # Flatten tensor into a batch of vectors
  # Input Tensor Shape: [batch_size, imageWidth/4, imageHeight/4, 64]
  # Output Tensor Shape: [batch_size, imageWidth/4 * imageHeight/4 * 64]
  pool2_flat = tf.reshape(pool2, [-1, (imageWidth//4) * (imageHeight//4) * 64])

  # Dense Layer
  # Densely connected layer with 1024 neurons
  # Input Tensor Shape: [batch_size, imageWidth/4 * imageHeight/4 * 64]
  # Output Tensor Shape: [batch_size, 1024]
  dense = tf.layers.dense(inputs=pool2_flat, units=1024, activation=tf.nn.relu)

  # Add dropout operation; 0.6 probability that element will be kept
  dropout = tf.layers.dropout(
      inputs=dense, rate=0.4, training=mode == tf.estimator.ModeKeys.TRAIN)

  # Logits layer
  # Input Tensor Shape: [batch_size, 1024]
  # Output Tensor Shape: [batch_size, 10]
  logits = tf.layers.dense(inputs=dropout, units=10)

  predictions = {
      # Generate predictions (for PREDICT and EVAL mode)
      "classes": tf.argmax(input=logits, axis=1),
      # Add `softmax_tensor` to the graph. It is used for PREDICT and by the
      # `logging_hook`.
      "probabilities": tf.nn.softmax(logits, name="softmax_tensor")
  }
  if mode == tf.estimator.ModeKeys.PREDICT:
    return tf.estimator.EstimatorSpec(mode=mode, predictions=predictions)

  # Calculate Loss (for both TRAIN and EVAL modes)
  loss = tf.losses.sparse_softmax_cross_entropy(labels=labels, logits=logits)

  # Configure the Training Op (for TRAIN mode)
  if mode == tf.estimator.ModeKeys.TRAIN:
    optimizer = tf.train.GradientDescentOptimizer(learning_rate=0.001)
    train_op = optimizer.minimize(
        loss=loss,
        global_step=tf.train.get_global_step())
    return tf.estimator.EstimatorSpec(mode=mode, loss=loss, train_op=train_op)

  # Add evaluation metrics (for EVAL mode)
  eval_metric_ops = {
      "accuracy": tf.metrics.accuracy(
          labels=labels, predictions=predictions["classes"])}
  return tf.estimator.EstimatorSpec(
      mode=mode, loss=loss, eval_metric_ops=eval_metric_ops)

def pos2str(pos):
	return '%02d:%02d:%02d:%02d' % (int(pos/(3600*30)),int(pos/(60*30)%60), int(pos/30%60), int(pos%30))

import argparse

def main():
	parser = argparse.ArgumentParser(description='do.py用のGUI')
	parser.add_argument('-n', '--target_number', default='1', help='target number')
	parser.add_argument('-i', '--image_file', help='image file')
	args = parser.parse_args()

	global TargetFileNumber
	TargetFileNumber = str(args.target_number)

	if args.image_file is not None:
		global TargetImageFile
		TargetImageFile = args.image_file

	app = wx.App()
	MyApp(None, wx.ID_ANY, 'Test PyGUI', size=(500,400))
	app.MainLoop()

if __name__ == '__main__':
	main()

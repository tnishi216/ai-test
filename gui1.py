#
#
# GUIのみのprogramにするための工事中script


import wx
import cv2
import numpy as np
import time
import random
from socket import gethostname

# C:/temp/test1.mp4
# 5:28 PG IN
# 59:28 CM IN
# 2:59:28 PG IN

# for debug
import matplotlib.pyplot as plt

# Internal modules
from marklist import MarkList
import dialogs

## Configuration ##

TargetFileNumber = '1'

TargetMp4File = "test"+TargetFileNumber+".mp4"
TargetMarkListFile = "marklist"+TargetFileNumber+".txt"
PredListFile = "predlist"+TargetFileNumber+".txt"

import os
os.environ['TF_CPP_MIN_LOG_LEVEL'] = '2'

LOG_ENABLED = False

# 学習用画像のサイズ
imageWidth = 56
imageHeight = 56

# 元画像のサイズ
orgWidth = imageWidth
orgHeight = imageHeight

imageChannel = 3
SHRINK_IMAGE = True
BATCH_SIZE = 100
INPUT_SIZE = 200

imageViewWidth = 278
imageViewHeight = 146

# Thumbnail size
tbWidth = 139
tbHeight = 73

# working values
pendingCount = 0

# Ref.: https://www.tensorflow.org/tutorials/estimators/cnn
# org github: https://github.com/tensorflow/tensorflow/blob/master/tensorflow/examples/tutorials/layers/cnn_mnist.py

# 参考：https://www.pyimagesearch.com/2017/02/06/faster-video-file-fps-with-cv2-videocapture-and-opencv/ (decode threading)

class MyApp(wx.Frame):

	def __init__(self, *args, **kw):
		super(MyApp, self).__init__(*args, **kw)
		self.learnInitialized = False
		self.bitmapImage = None

		self.PosList = []
		self.TypeList = {}

		self.PredPosList = []
		self.PredTypeList = {}

		# File operation
		self.edited = False
		self.filename = TargetMarkListFile

		self.init_ui()

	def init_ui(self):

		self.SetSize(800, 600)

		# Main Menu
		menu = wx.MenuBar()
		# File menu
		file = wx.Menu()
		self.Bind(wx.EVT_MENU, self.onFileNew, file.Append(-1, "&New"))
		self.Bind(wx.EVT_MENU, self.onFileLoad, file.Append(2, "&Load"))
		self.Bind(wx.EVT_MENU, self.onFileSave, file.Append(3, "&Save"))
		self.Bind(wx.EVT_MENU, self.onFileSaveAs, file.Append(4, "Save &As"))
		file.AppendSeparator()
		self.Bind(wx.EVT_MENU, self.onFileExit, file.Append(5, "E&xit"))
		# Edit menu
		edit = wx.Menu()
		self.Bind(wx.EVT_MENU, self.onEditUndo, file.Append(-1, "&Undo mark"))
		# Tool menu
		
		menu.Append(file, "&File")
		self.SetMenuBar(menu)

		# root panel
		pnlRoot = wx.Panel(self, wx.ID_ANY)

		# panelの生成
		pnlLeft = wx.Panel(pnlRoot, wx.ID_ANY)

		self.pnlImage = wx.Panel(pnlLeft, -1, pos=(10, 10), size=(imageViewWidth, imageViewHeight))

		pnlStatus = wx.Panel(pnlLeft, -1)

		panel = wx.Panel(pnlLeft, -1)

		# Build status panel
		layout = wx.BoxSizer(wx.HORIZONTAL)
		self.stxStatus = wx.StaticText(pnlStatus, -1, '00:00:00:00')
		layout.Add(self.stxStatus)
		self.stxInfo = wx.StaticText(pnlStatus, -1, '')
		layout.Add(self.stxInfo)
		pnlStatus.SetSizer(layout)

		layout = wx.BoxSizer(wx.HORIZONTAL)

		# buttonの生成
		#self.btnLoad = self.createButton(panel, 'Load', layout, self.load)
		self.btnStepBack = wx.Button(panel, wx.ID_ANY, 'Back')
		self.btnStepNext = wx.Button(panel, wx.ID_ANY, 'Next')
		self.btnBackSec = wx.Button(panel, wx.ID_ANY, 'BackSec')
		self.btnNextSec = wx.Button(panel, wx.ID_ANY, 'NextSec')

		# layoutの生成 for buttons
		layout.Add( self.btnStepBack )
		layout.Add( self.btnStepNext )
		layout.Add( self.btnBackSec )
		layout.Add( self.btnNextSec )

		# event handlerのbind
		self.btnStepBack.Bind(wx.EVT_BUTTON, self.stepBack)
		#self.btnStepBack.Bind(wx.EVT_LEFT_DOWN, self.stepBackLeftDown)
		#self.btnStepBack.Bind(wx.EVT_LEFT_UP, self.stepBackLeftUp)
		self.btnStepNext.Bind(wx.EVT_BUTTON, self.stepNext)
		self.btnBackSec.Bind(wx.EVT_BUTTON, self.backSec)
		self.btnNextSec.Bind(wx.EVT_BUTTON, self.nextSec)

		# layout設定
		panel.SetSizer(layout)

		# 二段目のbutton creation
		layout = wx.BoxSizer(wx.HORIZONTAL)
		panel2 = wx.Panel(pnlLeft, -1)
		self.btnBack15Sec = wx.Button(panel2, wx.ID_ANY, 'Back15Sec')
		self.btnNext15Sec = wx.Button(panel2, wx.ID_ANY, 'Next15Sec')
		self.btnPlayFF = wx.Button(panel2, wx.ID_ANY, 'PlayFF')
		layout.Add( self.btnBack15Sec )
		layout.Add( self.btnNext15Sec )
		layout.Add( self.btnPlayFF )
		self.btnBack15Sec.Bind(wx.EVT_BUTTON, self.back15Sec)
		self.btnNext15Sec.Bind(wx.EVT_BUTTON, self.next15Sec)
		self.btnPlayFF.Bind(wx.EVT_BUTTON, self.playFF)
		panel2.SetSizer(layout)

		# 三段目のbutton creation
		layout = wx.BoxSizer(wx.HORIZONTAL)
		panel3 = wx.Panel(pnlLeft, -1)
		self.createButton(panel3, 'Type &1', layout, self.type1)
		self.createButton(panel3, 'Type &2', layout, self.type2)
		self.createButton(panel3, 'Type &0', layout, self.type0)
		panel3.SetSizer(layout)

		layout = wx.BoxSizer(wx.HORIZONTAL)
		panel4 = wx.Panel(pnlLeft, -1)
		panel4.SetSizer(layout)

		layout = wx.BoxSizer(wx.HORIZONTAL)
		panel5 = wx.Panel(pnlLeft, -1)
		self.stxProgress = wx.StaticText(panel5, -1, 'xxxxxxxxxxxxxxxx')
		layout.Add(self.stxProgress)
		panel5.SetSizer(layout)

		# edit側のlayout
		layout = wx.BoxSizer(wx.VERTICAL)
		layout.Add(self.pnlImage)
		layout.Add(pnlStatus)
		layout.Add(panel)
		layout.Add(panel2)
		layout.Add(panel3)
		layout.Add(panel4)
		layout.Add(panel5)
		pnlLeft.SetSizer(layout)

		# 右側のmark image list
		pnlRight = wx.Panel(pnlRoot, wx.ID_ANY)

		layout = wx.BoxSizer(wx.VERTICAL)
		self.lcMarkImageList = wx.ListCtrl(pnlRight, wx.ID_ANY, size=(tbWidth+60,tbHeight*8), style=wx.LC_ICON)
		self.lcMarkImageList.Bind(wx.EVT_LIST_ITEM_SELECTED, self.evMarkImageList)
		layout.Add(self.lcMarkImageList)

		pnlRight.SetSizer(layout)

		# さらに右側のpredict結果一覧
		pnlPred = wx.Panel(pnlRoot, wx.ID_ANY)

		layout = wx.BoxSizer(wx.VERTICAL)
		self.lcPredImageList = wx.ListCtrl(pnlPred, wx.ID_ANY, size=(tbWidth+60,tbHeight*8), style=wx.LC_ICON)
		self.lcPredImageList.Bind(wx.EVT_LIST_ITEM_SELECTED, self.evPredImageList)
		layout.Add(self.lcPredImageList)

		pnlPred.SetSizer(layout)

		# 全体のlayout
		layout = wx.BoxSizer(wx.HORIZONTAL)
		layout.Add(pnlLeft)
		layout.Add(pnlRight)
		layout.Add(pnlPred)
		pnlRoot.SetSizer(layout)

		layout.Fit(pnlRoot)

		"""
		# easy version
		self.btn = wx.Button(panel, -1, 'Load', pos=(2,5))
		self.btn.Bind(wx.EVT_BUTTON, self.load)

		self.btnStepBack = wx.Button(panel, -1, 'Back', pos=(100,5))
		self.btnStepBack.Bind(wx.EVT_BUTTON, self.stepBack)

		self.btnStepNext = wx.Button(panel, -1, 'Next', pos=(200,5))
		self.btnStepNext.Bind(wx.EVT_BUTTON, self.stepNext)
		"""

		self.Show()

		self.load(None)

		self.loadMarkList()
		self.loadPredList()

#		self.testTrain()

	def onFileNew(self, event):
		if self.closeQuery() != False:
			return
		self.filename = ''

	def onFileLoad(self, event):
		self.filename = TargetMarkListFile
		self.loadMarkList()

	def onFileSave(self, event):
		self.saveMarkList()
		self.saveTrain()
	
	def onFileSaveAs(self, event):
		pass

	def onFileExit(self, event):
		if self.closeQuery() == False:
			return
		self.Close()

	def closeQuery(self):
		if self.edited:
			dlg = wx.MessageBox(None, "Mark List is not saved yet. Are you sure to exit?", "Exit", wx.YES_NO_CANCEL | wx.ICON_QUESTION)
			result = dlg.ShowModal()
			if result == wx.YES:
				self.onFileSaveAs()
			elif result == wx.NO:
				return True
			return False
		return True

	def onEditUndo(self, event):
		pass

	def createButton(self, panel, title, layout, handler):
		button = wx.Button(panel, -1, title)
		layout.Add( button )
		button.Bind(wx.EVT_BUTTON, handler)
		return button

	def load(self, event):
		if gethostname() == 'nishikawat-PC':
			FileName = "R:/temp/prs59i.mp4"		# 480x270 x3
		else:
			#FileName = "Q:/cap/test1.mp4"		# 268x148 x3
			FileName = "./image/"+TargetMp4File	# 268x148 x3
			# FileName = "Q:/cap/amarecco20180415-173633[000].avi"
		if not os.path.exists(FileName):
			print("No file: ", FileName)
			exit()
		self.cap = cv2.VideoCapture(FileName)
		global imageWidth, imageHeight
		global orgWidth, orgHeight
		orgWidth = int(self.cap.get(cv2.CAP_PROP_FRAME_WIDTH))
		orgHeight = int(self.cap.get(cv2.CAP_PROP_FRAME_HEIGHT))
		if not SHRINK_IMAGE:
			imageWidth = orgWidth
			imageHeight = orgHeight
		
		# Show Info
		text = "%d x %d %dfps" % (orgWidth, orgHeight, self.fps())
		self.stxInfo.SetLabel(text)
		
		# Resize controls
		global tbWidth, tbHeight
		tbWidth = orgWidth / 2
		tbHeight = orgHeight / 2
		self.pnlImage.SetSize(orgWidth, orgHeight);
		self.lcMarkImageList.SetSize(tbWidth+60, tbHeight*8)
		
		self.setFrame()

	def loadMarkList(self):
		if self.filename != '':
			marklist = MarkList()
			marklist.load(self.filename)
			self.PosList = marklist.frames
			self.TypeList = marklist.types
		self.updateMarkList()

	def saveMarkList(self):
		if self.filename == '':
			return
		marklist = MarkList()
		marklist.frames = self.PosList
		marklist.types = self.TypeList
		marklist.save(self.filename)

	def evMarkImageList(self, event):
		select_index = self.lcMarkImageList.GetFirstSelected()
		pos = self.PosList[select_index]
		self.setPos( pos )
		self.setFrame()

	def updateMarkList(self):
		imgList = wx.ImageList(tbWidth, tbHeight)

		for pos in self.PosList:
			bmp = self.getBitmapFrame(pos)
			img = bmp.ConvertToImage()
			img = img.Scale(tbWidth, tbHeight)
			imgList.Add( img.ConvertToBitmap() )
		self.lcMarkImageList.AssignImageList(imgList, wx.IMAGE_LIST_NORMAL)

		self.lcMarkImageList.ClearAll()

		i = 0
		for pos in self.PosList:
			type = self.TypeList[pos]
			title = pos2str(pos) + " " + str(type)
			self.lcMarkImageList.InsertItem(i, title, i)
			i += 1


	def loadPredList(self):
		predlist = MarkList()
		predlist.load(PredListFile)
		self.PredPosList = predlist.frames
		self.PredTypeList = predlist.types
		self.updatePredImageList()

	def savePredList(self):
		predlist = MarkList()
		predlist.frames = self.PredPosList
		predlist.types = self.PredTypeList
		predlist.save(PredListFile)


	def stepBack(self, event):
		self.setPos( self.getPos() - 2 )
		self.setFrame()

	def stepBackLeftDown(self, event):
		super
		print("Left Down")

	def stepBackLeftUp(self, event):
		super
		print("Left Up")

	def stepNext(self, event):
		self.setFrame()

	def backSec(self, event):
		pos = self.getPos()
		if pos == 0:
			return
		pos = pos - self.fps() - 1
		if pos < 0:
			pos = 0
		self.setPos( pos )
		self.setFrame()

	def nextSec(self, event):
		pos = self.getPos()
		if pos == self.getFrameCount()-1:
			return
		pos = pos + self.fps() - 1
		if pos >= self.getFrameCount():
			pos = self.getFrameCount() - 1
		self.setPos( pos )
		self.setFrame()

	def back15Sec(self, event):
		pos = self.getPos()
		if pos == 0:
			return
		pos = pos - self.fps()*15 - 1
		if pos < 0:
			pos = 0
		self.setPos( pos )
		self.setFrame()

	def next15Sec(self, event):
		pos = self.getPos()
		if pos == self.getFrameCount()-1:
			return
		pos = self.getPos() + self.fps()*15 - 1
		if pos >= self.getFrameCount():
			pos = self.getFrameCount()-1
		self.setPos( pos )
		self.setFrame()

	def playFF(self, event):
		pass

	def type1(self, evet):
		self.setType(1)

	def type2(self, evet):
		self.setType(2)

	def type0(self, evet):
		self.setType(0)

	def setType(self, type):
		pos = self.getPos()
		self.PosList.append(pos)
		self.TypeList[pos] = type
		self.updateMarkList()

	def predict(self, event):
		#self.evalProc(self.getPos())
		self.predictProc(self.getPos())

	def setFrame(self):
		pos = self.getPos()
		self.setPos(pos)
		ret, frame = self.cap.read()	# getPosが１つ進む
		if not ret:
			return
		self.setStatusLabel(int(pos))
		frame = cv2.cvtColor(frame, cv2.COLOR_BGR2RGB)
		img = wx.Bitmap.FromBuffer(frame.shape[1], frame.shape[0], frame)
		if self.bitmapImage is None:
			self.bitmapImage = wx.StaticBitmap(self.pnlImage, -1, img, pos=(0,0), size=(orgWidth, orgHeight))
		else:
			self.bitmapImage.SetBitmap( img )

	def getBitmapFrame(self, pos):
		orgpos = self.getPos()
		self.setPos(pos)
		ret, frame = self.cap.read()	# getPosが１つ進む
		if not ret:
			return
		frame = cv2.cvtColor(frame, cv2.COLOR_BGR2RGB)
		img = wx.Bitmap.FromBuffer(frame.shape[1], frame.shape[0], frame)
		self.setPos(orgpos)
		return img

	def setStatusLabel(self, pos):
		str = pos2str(pos)
		self.stxStatus.SetLabel(str)

	def updatePredImageList(self):
		imgList = wx.ImageList(tbWidth, tbHeight)

		for pos in self.PredPosList:
			bmp = self.getBitmapFrame(pos)
			img = bmp.ConvertToImage()
			img = img.Scale(tbWidth, tbHeight)
			imgList.Add( img.ConvertToBitmap() )
		self.lcPredImageList.AssignImageList(imgList, wx.IMAGE_LIST_NORMAL)

		self.lcPredImageList.ClearAll()

		i = 0
		for pos in self.PredPosList:
			type = self.PredTypeList[pos]
			title = pos2str(pos) + " " + str(type)
			self.lcPredImageList.InsertItem(i, title, i)
			i += 1

	def evPredImageList(self, event):
		select_index = self.lcPredImageList.GetFirstSelected()
		pos = self.PredPosList[select_index]
		self.setPos( pos )
		self.setFrame()

# -------------------------
#	source image情報

	def fps(self):
		return int(self.cap.get( cv2.CAP_PROP_FPS ))

	def getPos(self):
		pos = self.cap.get( cv2.CAP_PROP_POS_FRAMES )
		return int(pos)

	def setPos(self, pos):
		self.cap.set( cv2.CAP_PROP_POS_FRAMES, pos )

	def getFrameCount(self):
		return int(self.cap.get( cv2.CAP_PROP_FRAME_COUNT ))

# -------------------------

	def getTypeByPosFromMarkList(self, pos):
		index = self.getPosListIndexByPosFromMarkList(pos)
		if index == -1:
			return -1
		return self.TypeList[self.PosList[index]]

	def getPosListIndexByPosFromMarkList(self, pos):
		result = None
		poslen = len(self.PosList)
		left = 0
		right = poslen
		if right == 0:
			return -1
		while left <= right:
			mid = (left + right) // 2
			if pos >= self.PosList[mid] and (mid == poslen-1 or pos < self.PosList[mid+1]):
				result = mid
				break
			if pos < self.PosList[mid]:
				right = mid - 1
			else:
				left = mid + 1
		if result == None:
			return -1
		else:
			return result

def pos2str(pos):
	return '%02d:%02d:%02d:%02d' % (int(pos/(3600*30)),int(pos/(60*30)%60), int(pos/30%60), int(pos%30))

def main():
	app = wx.App()
	MyApp(None, wx.ID_ANY, 'Test PyGUI', size=(500,400))
	app.MainLoop()

if __name__ == '__main__':
	main()

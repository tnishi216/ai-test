
# cross validation

prev=0

for target in `seq 1 12`; do
	if [ $target -ne $1 ]; then
		if [ $prev -eq 0 ]; then
			python3 keras_cnn3.py train_eval -n $target -s keras_cnn_model${target}
		else
			python3 keras_cnn3.py train_eval -n $target -l keras_cnn_model${prev} -s keras_cnn_model${target}
		fi
		cp keras_cnn.png keras_cnn${target}.png
		prev=$target
	fi
done


for target in `seq 1 12`; do
	if [ $target -ne $1 ]; then
		python3 keras_cnn3.py eval -n $1 -l keras_cnn_model${target}
	fi
done

#python3 keras_cnn3.py pred -n 12 -l keras_cnn_model


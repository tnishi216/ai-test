

python3 keras_cnn3.py train_eval -n 1 -s keras_cnn_model
cp keras_cnn_model keras_cnn_model1
cp keras_cnn.png keras_cnn1.png
python3 keras_cnn3.py train_eval -n 2 -l keras_cnn_model -s keras_cnn_model
cp keras_cnn_model keras_cnn_model2
cp keras_cnn.png keras_cnn2.png
python3 keras_cnn3.py train_eval -n 3 -l keras_cnn_model -s keras_cnn_model
cp keras_cnn_model keras_cnn_model3
cp keras_cnn.png keras_cnn3.png
python3 keras_cnn3.py eval -n 4 -l keras_cnn_model1
python3 keras_cnn3.py eval -n 4 -l keras_cnn_model2
python3 keras_cnn3.py eval -n 4 -l keras_cnn_model3

#python3 keras_cnn3.py pred -n 4 -ld keras_cnn4 -l keras_cnn_model


#
#
#

import argparse
import shutil
import subprocess
import os

def remove(file):
	if os.path.exists(file):
		os.remove(file)

parser = argparse.ArgumentParser()

parser.add_argument('-n', '--number_of_images', default='1', help='the number of sample images')

args = parser.parse_args()

nr_images = int(args.number_of_images)

remove("keras_cnn3.log")

for cur_target in range(1, nr_images+1):
	prev = 0

	for target in range(1, nr_images+1):
		if target != cur_target:
			if prev == 0:
				print("---- keras_cnn3.py train_eval -n "+str(target)+" -s keras_cnn_model"+str(target))
				subprocess.call(["python3", "keras_cnn3.py", "train_eval", "-n", str(target), "-s", "keras_cnn_model"+str(target)])
			else:
				print("---- keras_cnn3.py train_eval -n "+str(target)+" -l keras_cnn_model"+str(prev)+" -s keras_cnn_model"+str(target))
				subprocess.call(["python3", "keras_cnn3.py", "train_eval", "-n", str(target), "-l", "keras_cnn_model"+str(prev), "-s", "keras_cnn_model"+str(target)])

			try:
				shutil.copy("keras_cnn.png", "keras_cnn"+str(target)+".png")
			except Exception as e:
				print(e)
			prev = target


	for target in range(1, nr_images+1):
		if target != cur_target:
			print("---- keras_cnn3.py eval -n "+str(cur_target)+" -l keras_cnn_model"+str(target))
			subprocess.call(["python3", "keras_cnn3.py", "eval",  "-n", str(cur_target), "-l", "keras_cnn_model"+str(target)])


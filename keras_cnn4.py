#
# keras_cnn3.pyを Google Colaboratory で使いやすくしたもの(keras_cnn3.pyの動作検証用)
#
#
# Ref: https://newtechnologylifestyle.net/cifar-10%E3%81%AE%E3%83%87%E3%83%BC%E3%82%BF%E3%82%BB%E3%83%83%E3%83%88%E3%82%92%E7%94%A8%E3%81%84%E3%81%A6cnn%E3%81%AE%E7%94%BB%E5%83%8F%E8%AA%8D%E8%AD%98%E3%82%92%E8%A1%8C%E3%81%A3%E3%81%A6%E3%81%BF/
# これをmp4からの学習に対応
#

import time
import numpy as np
import matplotlib.pyplot as plt
import cv2
import os
from socket import gethostname

from keras.datasets import mnist
from keras.models import Sequential
from keras.layers import Dense, Dropout, Activation, Flatten
from keras.layers.convolutional import Conv2D, MaxPooling2D
from keras.optimizers import Adam
from keras.utils import np_utils, to_categorical
from keras.models import load_model
from keras.utils import plot_model

import struct


# Internal modules
from marklist import MarkList

# Logger
from logger import Logger
logger = Logger('keras_cnn')

#### Global Variables ####

## Configuration ##

TargetFileNumber = '1'

# 学習用画像のサイズ
imageWidth = 112
imageHeight = 112

# 元画像のサイズ
orgWidth = imageWidth
orgHeight = imageHeight

imageChannel = 3
labelCount = 2

BATCH_SIZE = 50
EPOCHS = 10

INPUT_SIZE_VIEW = 200	# viewOriginal()での表示画像数

SKIP_FRAMES = 5
VALID_IS_TRAIN = True	# 検証データは学習データ

imageViewWidth = 278
imageViewHeight = 146

SC_GUARD_FRAMES = 2		# scene changeの前後frame数は学習に使用しない(画乱れがあるため)

# 最大学習・予測時間
maxSec = 0	# 0 means all

class MyApp:

	def __init__(self, *args, **kw):
		super(MyApp, self).__init__(*args, **kw)
		self.learnInitialized = False
		self.bitmapImage = None

		TargetMarkListFile = "marklist"+TargetFileNumber+".txt"

		self.PosList = []
		self.TypeList = {}

		self.PredPosList = []
		self.PredTypeList = {}

		# File operation
		self.edited = False
		self.filename = TargetMarkListFile

		self.load()

		self.loadMarkList()

	def load(self):
		TargetMp4File = "./image/test"+TargetFileNumber+".mp4"
		FileName = TargetMp4File	# 268x148 x3

		if not os.path.exists(FileName):
			logger.error("No file: " + FileName)
			exit()
		self.cap = cv2.VideoCapture(FileName)
		global imageWidth, imageHeight
		global orgWidth, orgHeight
		orgWidth = int(self.cap.get(cv2.CAP_PROP_FRAME_WIDTH))
		orgHeight = int(self.cap.get(cv2.CAP_PROP_FRAME_HEIGHT))
		
		# Show Info
		text = "%d x %d %dfps" % (orgWidth, orgHeight, self.fps())
		logger.info(text)

	def loadMarkList(self):
		if self.filename != '':
			marklist = MarkList()
			if not marklist.load(self.filename):
				print("Failed to load marklist!!!")
				exit(1)
			self.PosList = marklist.frames
			self.TypeList = marklist.types

	def initLearn(self, filename=None):
		if self.learnInitialized:
			return

		if filename:
			# load model from file
			self.model = load_model(filename)
			logger.info("loaded model: {}".format(filename))
		else:
			model = Sequential()

			model.add(Conv2D(32, (3, 3), padding='same',input_shape=(imageWidth, imageHeight, imageChannel)))
			model.add(Activation('relu'))
			model.add(Conv2D(32, (3, 3)))
			model.add(Activation('relu'))
			model.add(MaxPooling2D(pool_size=(2, 2)))
			model.add(Dropout(0.25))
			 
			model.add(Conv2D(64, (3, 3), padding='same'))
			model.add(Activation('relu'))
			model.add(Conv2D(64, (3, 3)))
			model.add(Activation('relu'))
			model.add(MaxPooling2D(pool_size=(2, 2)))
			model.add(Dropout(0.25))
			 
			model.add(Flatten())
			model.add(Dense(512))
			model.add(Activation('relu'))
			model.add(Dropout(0.5))
			model.add(Dense(labelCount))
			model.add(Activation('softmax'))

			model.compile(optimizer='rmsprop',
              loss='binary_crossentropy',
              metrics=['accuracy'])

			print(model.summary())

			self.model = model

		self.learnInitialized = True

	# input:
	#	maxSec(global)
	# output:
	#	train_data, train_labels, count, start_pos, curpos
	def prepareInputData(self, skip_frames=3):
		print("Preparing data...");
		# 各typeのframe数の計算
		numlist = self.calcNumList()
		count = 0
		start_pos = 0
		end = False
		
		total_count = 0
		skip_data = 400

		if skip_frames <= SC_GUARD_FRAMES:
			print("Not supported skip_frames <= SC_GUARD_FRAMES")
			exit(1)

		# まず、必要な入力画像数を求める(配列の確保に必要なため - 動的にできるのなら不要な処理）
		for pos, numframe in zip(self.PosList, numlist):
			if end:
				break
			type = self.TypeList[pos]
			guard_frames = SC_GUARD_FRAMES
			for frame_index in range(numframe):
				if frame_index % skip_frames != 0:
					continue

				if maxSec != 0 and pos + frame_index > maxSec * self.fps():
					end = True
					break

				read_frame = pos + frame_index + guard_frames
				guard_frames = 0
				if frame_index + SC_GUARD_FRAMES >= numframe:
					# 現在のtypeの最終フレームがguard frame数内にある場合はskip
					break

				if count==0:
					start_pos = pos + frame_index

				count += 1

		curpos = pos + frame_index

		input_count = count
		logger.info("input_count=%d" % (input_count))

		count = 0

		#data_size = imageWidth * imageHeight * imageChannel
		train_data = np.empty((input_count, imageWidth, imageHeight, imageChannel), dtype=np.float32)
		train_labels = np.empty((input_count, 1), dtype=np.int32)

		end = False

#		print("PosList={} numlist={}".format(self.PosList, numlist))

		# 学習データの構築
		for pos, numframe in zip(self.PosList, numlist):
#			print("pos={} numframe={}".format(pos, numframe))
			if end:
				break
			type = self.TypeList[pos]
			guard_frames = SC_GUARD_FRAMES
			for frame_index in range(numframe):
				if frame_index % skip_frames != 0:
					continue

				if maxSec != 0 and pos + frame_index > maxSec * self.fps():
					end = True
					break

				read_frame = pos + frame_index + guard_frames
				guard_frames = 0
				if frame_index + SC_GUARD_FRAMES >= numframe:
					# 現在のtypeの最終フレームがguard frame数内にある場合はskip
					break

				#print("read_frame=", read_frame)
				self.setPos(read_frame)
				ret, frame = self.cap.read()	# getPosが１つ進む
				if not ret:
					break

				if count==0:
					start_pos = pos + frame_index

				# Build the input data
				frame = cv2.resize(frame, (imageWidth, imageHeight))

				data = frame.astype(np.float32)/255.0

				train_data[count] = data.reshape(1, imageWidth, imageHeight, imageChannel)

				train_labels[count][0] = type - 1		# 1,2 → 0,1

				if count != 0 and count % 1000 == 0:
					str = "preparing: %d" % (count)
					print(str)

				count += 1

		if count != input_count:
			logger.error("count and input_count not matched!!! count={} input_count={}".format(count, input_count))
			exit(1)

		curpos = pos + frame_index

		print("Preparing data done. count={}".format(count));

		return train_data, train_labels, count, start_pos, curpos

	def trainStart(self, model_filename=None):
		self.initLearn(model_filename)

		start_time = time.time()

		train_data, train_labels, count, start_pos, curpos = self.prepareInputData(skip_frames=SKIP_FRAMES)

		start_tc = pos2str(start_pos)
		str = "--- Training: [%s] %s %d - %d (%d step) EPOCH=%d BATCH=%d" % (TargetFileNumber, start_tc, start_pos, curpos, SKIP_FRAMES, EPOCHS, BATCH_SIZE)
		logger.info(str)
		self.trainBatch(train_data, train_labels, count)

		elapsed_time = time.time() - start_time
		logger.info("-- End of training : (%d[sec])" % (elapsed_time))

	def trainBatch(self, x_train, y_train, data_count):
		y_train = to_categorical(y_train, labelCount)

		if not VALID_IS_TRAIN:
			# split off last data_count/2 training samples for validation
			logger.info('Use half data for valid')
			x_train, x_valid = np.split(x_train, [int(data_count/2)])
			y_train, y_valid = np.split(y_train, [int(data_count/2)])
		else:
			# 学習データと評価データを同じにする
			logger.info('Use full data for valid')
			x_valid = x_train
			y_valid = y_train

		#print("start fit")
		self.fit = self.model.fit(
		    x_train, y_train,
		    batch_size=BATCH_SIZE,
		    epochs=EPOCHS)
		# verbose=2, callback=[] で静かになる(速くなる)

	def eval(self, filename=None):
		self.initLearn(filename)

		start_time = time.time()

		x_valid, y_valid, count, start_pos, curpos = self.prepareInputData(skip_frames=SKIP_FRAMES)

		start_tc = pos2str(start_pos)
		str = "--- Evaluation:[%s] %s %d - %d (%d count, %d step)" % (TargetFileNumber, start_tc, start_pos, curpos, count, SKIP_FRAMES)
		logger.info(str)

		# convert class labels to one-hot encodings
		y_valid = to_categorical(y_valid, labelCount)

		print("start evaluate")
		score = self.model.evaluate(
		    x_valid, y_valid,
		    batch_size=BATCH_SIZE,
		    verbose=2)

		elapsed_time = time.time() - start_time
		logger.info("-- End of evaluation : (%d[sec])" % (elapsed_time))
		logger.info("loss=%g accuracy=%g" % (score[0], score[1]))
		print("loss=", score[0])
		print("accuracy=", score[1])

	def calcNumList(self):
		numlist = []
		prev_pos = -1
		for pos in self.PosList:
			if prev_pos != -1:
				numlist.append(pos - prev_pos)
			prev_pos = pos
		if prev_pos == -1:
			prev_pos = 0
		numlist.append(self.getFrameCount() - prev_pos)
		return numlist

	def saveModel(self, filename):
		self.model.save(filename)

	def printModel(self, model_filename=None):
		self.initLearn(model_filename)

		print("Output to model.png")
		plot_model(self.model, to_file='model.png',show_shapes=True)

	#
	# Make Data
	#
	def makeData(self):
		self.prepareInputData(skip_frames=SKIP_FRAMES)

# -------------------------
#	source image情報

	def fps(self):
		return int(self.cap.get( cv2.CAP_PROP_FPS ))

	def getPos(self):
		pos = self.cap.get( cv2.CAP_PROP_POS_FRAMES )
		return int(pos)

	def setPos(self, pos):
		self.cap.set( cv2.CAP_PROP_POS_FRAMES, pos )

	def getFrameCount(self):
		return int(self.cap.get( cv2.CAP_PROP_FRAME_COUNT ))

def pos2str(pos):
	return '%02d:%02d:%02d:%02d' % (int(pos/(3600*30)),int(pos/(60*30)%60), int(pos/30%60), int(pos%30))

def main():
	logger.info(" === Start === %s" % (TargetFileNumber))

	app = MyApp()

	app.trainStart()
	app.eval()

if __name__ == '__main__':
	main()

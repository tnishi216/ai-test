
# cross validation

prev=0

for target in `seq 1 9`; do
	if [ $target -ne $1 ]; then
		if [ $prev -eq 0 ]; then
			python3 keras_cnn5.py train_eval -n $target -s keras_cnn5_model${target}
		else
			python3 keras_cnn5.py train_eval -n $target -l keras_cnn5_model${prev} -s keras_cnn5_model${target}
		fi
		cp keras_cnn5.png keras_cnn5${target}.png
		prev=$target
	fi
done


for target in `seq 1 9`; do
	if [ $target -ne $1 ]; then
		python3 keras_cnn5.py eval -n $1 -l keras_cnn5_model${target}
	fi
done

#python3 keras_cnn5.py pred -n 9 -l keras_cnn5_model


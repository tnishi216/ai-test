#
# Ref: https://newtechnologylifestyle.net/cifar-10%E3%81%AE%E3%83%87%E3%83%BC%E3%82%BF%E3%82%BB%E3%83%83%E3%83%88%E3%82%92%E7%94%A8%E3%81%84%E3%81%A6cnn%E3%81%AE%E7%94%BB%E5%83%8F%E8%AA%8D%E8%AD%98%E3%82%92%E8%A1%8C%E3%81%A3%E3%81%A6%E3%81%BF/
# これをmp4からの学習に対応
# ここから、cifar10用に改造
#
# 実行例：
#	１つ目の動画ファイルを学習、modelの保存、入力データの保存
#	keras_cnn.py train -n 1 -s model1 -sd data1
#	その学習データを使って評価(loss=0 accu=1になるはず)、入力データは前回保存したものを利用
#	keras_cnn.py eval -n 1 -l model1 -ld data1
#	さらに、そのmodelに追加学習、入力データの保存
#	keras_cnn.py train -n 2 -l model1 -s model1 -sd data2
#
# ※trainのときのみ、keras_cnn.pngに学習曲線が出力される

import time
import numpy as np
import matplotlib.pyplot as plt
import cv2
import os
from socket import gethostname

from keras.datasets import mnist
from keras.models import Sequential
from keras.layers import Dense, Dropout, Activation, Flatten
from keras.layers.convolutional import Conv2D, MaxPooling2D
from keras.optimizers import Adam
from keras.utils import np_utils, to_categorical
from keras.models import load_model
from keras.utils import plot_model
from keras.datasets import cifar10

import struct

# Internal modules
from marklist import MarkList

# Logger
from logger import Logger
logger = Logger('keras_cnn')

#(X_train, y_train), (X_test, y_test) = cifar10.load_data()

#### Global Variables ####

## Configuration ##

TargetFileNumber = '1'

TargetMp4File = "test"+TargetFileNumber+".mp4"
TargetMarkListFile = "marklist"+TargetFileNumber+".txt"

# 学習用画像のサイズ
imageWidth = 32
imageHeight = 32

# 元画像のサイズ
orgWidth = imageWidth
orgHeight = imageHeight

imageChannel = 3
labelCount = 10
SHRINK_IMAGE = True
BATCH_SIZE = 100
EPOCHS = 50
INPUT_SIZE = 200

SKIP_FRAMES = 5
VALID_IS_TRAIN = True	# 検証データは学習データ

imageViewWidth = 278
imageViewHeight = 146

# Thumbnail size
tbWidth = 139
tbHeight = 73

# working values
pendingCount = 0

# 最大学習・予測時間
maxSec = 0	# 0 means all

class MyApp:

	def __init__(self, *args, **kw):
		super(MyApp, self).__init__(*args, **kw)
		self.learnInitialized = False
		self.bitmapImage = None

		global TargetMp4File
		global TargetMarkListFile
		TargetMp4File = "test"+TargetFileNumber+".mp4"
		TargetMarkListFile = "marklist"+TargetFileNumber+".txt"

		self.PosList = []
		self.TypeList = {}

		self.PredPosList = []
		self.PredTypeList = {}

		# File operation
		self.edited = False
		self.filename = TargetMarkListFile

		self.load()

		self.loadMarkList()

	def load(self):
		if gethostname() == 'nishikawat-PC':
			FileName = "R:/temp/prs59i.mp4"		# 480x270 x3
		else:
			#FileName = "Q:/cap/test1.mp4"		# 268x148 x3
			FileName = "./image/"+TargetMp4File	# 268x148 x3
			# FileName = "Q:/cap/amarecco20180415-173633[000].avi"
		if not os.path.exists(FileName):
			logger.error("No file: " + FileName)
			exit()
		self.cap = cv2.VideoCapture(FileName)
		global imageWidth, imageHeight
		global orgWidth, orgHeight
		orgWidth = int(self.cap.get(cv2.CAP_PROP_FRAME_WIDTH))
		orgHeight = int(self.cap.get(cv2.CAP_PROP_FRAME_HEIGHT))
		if not SHRINK_IMAGE:
			imageWidth = orgWidth
			imageHeight = orgHeight
		
		# Show Info
		text = "%d x %d %dfps" % (orgWidth, orgHeight, self.fps())
		logger.info(text)
		
		# Resize controls
		global tbWidth, tbHeight
		tbWidth = orgWidth / 2
		tbHeight = orgHeight / 2

	def loadMarkList(self):
		if self.filename != '':
			marklist = MarkList()
			if not marklist.load(self.filename):
				print("Failed to load marklist!!!")
				exit(1)
			self.PosList = marklist.frames
			self.TypeList = marklist.types


	def initLearn(self, filename=None):
		if self.learnInitialized:
			return

		if filename:
			# load model from file
			self.model = load_model(filename)
		else:
			model = Sequential()

			model.add(Conv2D(32, (3, 3), padding='same',input_shape=(imageWidth, imageHeight, imageChannel)))
			model.add(Activation('relu'))
			model.add(Conv2D(32, (3, 3)))
			model.add(Activation('relu'))
			model.add(MaxPooling2D(pool_size=(2, 2)))
			model.add(Dropout(0.25))
			 
			model.add(Conv2D(64, (3, 3), padding='same'))
			model.add(Activation('relu'))
			model.add(Conv2D(64, (3, 3)))
			model.add(Activation('relu'))
			model.add(MaxPooling2D(pool_size=(2, 2)))
			model.add(Dropout(0.25))
			 
			model.add(Flatten())
			model.add(Dense(512))
			model.add(Activation('relu'))
			model.add(Dropout(0.5))
			model.add(Dense(labelCount))
			model.add(Activation('softmax'))

			model.compile(optimizer='rmsprop',
              loss='binary_crossentropy',
              metrics=['accuracy'])

			#model.compile(
			#    loss='categorical_crossentropy',
			#    optimizer=Adam(lr=1e-3),
			#    metrics=['accuracy'])

			print(model.summary())

			self.model = model

			# Set up logging for predictions
			# Log the values in the "Softmax" tensor with label "probabilities"
	#		tensors_to_log = {"probabilities": "softmax_tensor"}
	#		self.logging_hook = tf.train.LoggingTensorHook(tensors=tensors_to_log, every_n_iter=50)

		self.learnInitialized = True

	# input:
	#	maxSec(global)
	# output:
	#	train_data, train_labels, count, start_pos, curpos
	def prepareInputData(self, skip_frames=3):
		# 各typeのframe数の計算
		numlist = self.calcNumList()
		count = 0
		start_pos = 0
		end = False
		
		total_count = 0
		skip_data = 400

		# まず、必要な入力画像数を求める(配列の確保に必要なため - 動的にできるのなら不要な処理）
		for pos, numframe in zip(self.PosList, numlist):
			if end:
				break
			type = self.TypeList[pos]
			for frame_index in range(numframe):
				if frame_index % skip_frames != 0:
					continue

				if maxSec != 0 and pos + frame_index > maxSec * self.fps():
					end = True
					break

#				total_count += 1
#				if total_count < skip_data:
#					continue

#				self.setPos(pos + frame_index)
#				ret, frame = self.cap.read()	# getPosが１つ進む
#				if not ret:
#					print("ret end")
#					break

				if count==0:
					start_pos = pos + frame_index

				count += 1

		curpos = pos + frame_index

		input_count = count
		logger.info("input_count=%d" % (input_count))

		if loadDataFileName == None:
			count = 0

			#data_size = imageWidth * imageHeight * imageChannel
			train_data = np.empty((0, imageWidth, imageHeight, imageChannel))
			train_labels = np.empty((input_count, 1), dtype=np.int32)

			end = False

			# 学習データの構築
			for pos, numframe in zip(self.PosList, numlist):
				if end:
					break
				type = self.TypeList[pos]
				for frame_index in range(numframe):
					if frame_index % skip_frames != 0:
						continue

					if maxSec != 0 and pos + frame_index > maxSec * self.fps():
						end = True
						break

	#				total_count += 1
	#				if total_count < skip_data:
	#					continue

					self.setPos(pos + frame_index)
					ret, frame = self.cap.read()	# getPosが１つ進む
					if not ret:
						break

					if count==0:
						start_pos = pos + frame_index

					# Build the input data
					frame = cv2.resize(frame, (imageWidth, imageHeight))
#					frame = np.rollaxis(frame, 0, 3)	# [[Redの配列],[Greenの配列],[Blueの配列]] に並び替える
#					print("frame=", frame.shape)

					data = frame.astype(np.float32)/255.0

					train_data = np.append(train_data, data.reshape(1, imageWidth, imageHeight, imageChannel), axis=0).astype(np.float32)	# row方向に１つ追加

					train_labels[count][0] = type - 1		# 1,2 → 0,1
	#				print("type=%d pos=%d" % (type, pos + frame_index))

					if count != 0 and count % 1000 == 0:
						str = "preparing: %d" % (count)
						logger.info(str)

					count += 1

			curpos = pos + frame_index
			
			if saveDataFileName != None:
				np.save(saveDataFileName + '_data.npy', train_data)
				np.save(saveDataFileName + '_label.npy', train_labels)
		else:
			# load data from file
			train_data = np.load(loadDataFileName + '_data.npy')
			train_labels = np.load(loadDataFileName + '_label.npy')

		return train_data, train_labels, count, start_pos, curpos

	def trainStart(self, model_filename=None, with_eval=False):
		self.initLearn(model_filename)

		start_time = time.time()

#		train_data, train_labels, count, start_pos, curpos = self.prepareInputData(skip_frames=SKIP_FRAMES)

		(X_train, y_train), (X_test, y_test) = cifar10.load_data()
		#(X_train, y_train) = cifar10.load_data()
#		global X_train
#		global y_train
		train_data = X_train.astype('float32') / 255.
		train_labels = y_train
		count = 50000
		start_pos = 0
		curpos = count

		start_tc = pos2str(start_pos)
#		print("train_data=", train_data.shape)
#		print("train_labels=", train_labels)
#		print("train_labels=", train_labels.shape)
		str = "--- Training: [%s] %s %d - %d (%d step)" % (TargetFileNumber, start_tc, start_pos, curpos, SKIP_FRAMES)
		logger.info(str)
		self.trainBatch(train_data, train_labels, count, with_eval)

		elapsed_time = time.time() - start_time
		logger.info("-- End of training : (%d[sec])" % (elapsed_time))

		if with_eval:
			# ----------------------------------------------
			# Some plots
			# ----------------------------------------------
			fig, (axL, axR) = plt.subplots(ncols=2, figsize=(10,4))

			self.plot_history_loss(self.fit, axL)
			self.plot_history_acc(self.fit, axR)
			fig.savefig('./keras_cnn.png')
			plt.close()

	def trainBatch(self, x_train, y_train, data_count, with_eval):
		# DEBUG: show the training data
		#fig, ax = plt.subplots(nrows=2, ncols=5, sharex=True, sharey=True)
		#ax = ax.flatten()
		#for i in range(10):
		#	img = train_data[i].reshape(28, 28) <- ここがうまくいかない
		#	ax[i].imshow(img, cmap='Greys')
		#	ax[0].set_xticks([])
		#	ax[0].set_yticks([])
		#	plt.tight_layout()
		#	plt.show()

		#
		# 前処理
		#

		# convert integer RGB values (0-255) to float values (0-1)
		#x_train = x_train.astype('float32') / 255
		#x_train = x_train / 255.

		# reshape the input data
#		x_test = x_test.reshape(10000, imageWidth*imageHeight*imageChannel)

		# convert class labels to one-hot encodings
		y_train = to_categorical(y_train, labelCount)
#		y_test = to_categorical(y_test, labelCount)

		if not VALID_IS_TRAIN:
			# split off last data_count/2 training samples for validation
			logger.info('Use half data for valid')
			x_train, x_valid = np.split(x_train, [int(data_count/2)])
			y_train, y_valid = np.split(y_train, [int(data_count/2)])
		else:
			# 学習データと評価データを同じにする
			logger.info('Use full data for valid')
			x_valid = x_train
			y_valid = y_train

		logger.info("start fit")
		self.fit = self.model.fit(
		    x_train, y_train,
		    batch_size=BATCH_SIZE,
		    epochs=EPOCHS,
		    verbose=2,
		    validation_data=(x_valid, y_valid) if with_eval else None,
		    callbacks=[])

	def eval(self, filename=None):
		self.initLearn(filename)

		start_time = time.time()

		x_valid, y_valid, count, start_pos, curpos = self.prepareInputData(skip_frames=SKIP_FRAMES)

		start_tc = pos2str(start_pos)
		str = "--- Evaluation:[%s] %s %d - %d (%d count, %d step)" % (TargetFileNumber, start_tc, start_pos, curpos, count, SKIP_FRAMES)
		logger.info(str)

		x_valid = x_valid.astype('float32') / 255

		# reshape the input data

		# convert class labels to one-hot encodings
		y_valid = to_categorical(y_valid, labelCount)

		logger.info("start evaluate")
		score = self.model.evaluate(
		    x_valid, y_valid,
		    batch_size=BATCH_SIZE,
		    verbose=2)

		elapsed_time = time.time() - start_time
		logger.info("-- End of evaluation : (%d[sec])" % (elapsed_time))
		logger.info("loss=%g accuracy=%g" % (score[0], score[1]))
		print("loss=", score[0])
		print("accuracy=", score[1])

	def predictAll(self, filename=None):
		self.initLearn(filename)
		
		(X_train, y_train), (X_test, y_test) = cifar10.load_data()
		#(X_train, y_train) = cifar10.load_data()
#		global X_train
#		global y_train
		X_train = X_train.astype('float32') / 255.
		print("data=", X_train.shape)
		eval_data = X_train[0:1]
		print("eval_data=", eval_data.shape)

		pred = self.model.predict(eval_data)
		print("pred=", pred)
		score = np.max(pred)
		print("score=", score)
		pred_label = np.argmax(pred)
		print("pred_label=", pred_label)

	# loss
	def plot_history_loss(self, fit, axL):
	    # Plot the loss in the history
	    axL.plot(fit.history['loss'],label="loss for training")
	    axL.plot(fit.history['val_loss'],label="loss for validation")
	    axL.set_title('model loss')
	    axL.set_xlabel('epoch')
	    axL.set_ylabel('loss')
	    axL.legend(loc='upper right')

	# acc
	def plot_history_acc(self, fit, axR):
	    # Plot the loss in the history
	    axR.plot(fit.history['acc'],label="loss for training")
	    axR.plot(fit.history['val_acc'],label="loss for validation")
	    axR.set_title('model accuracy')
	    axR.set_xlabel('epoch')
	    axR.set_ylabel('accuracy')
	    axR.legend(loc='upper right')

	def eval_model(self, eval_data, eval_labels):
		# Evaluate the model and print results
		eval_input_fn = tf.estimator.inputs.numpy_input_fn(
			x={"x": eval_data}, y=eval_labels, num_epochs=1, shuffle=False)
		eval_results = self.classifier.evaluate(input_fn=eval_input_fn)
		print(eval_results)	

	def predictProc(self, pos):
		self.initLearn()

		self.setPos(pos)
		ret, frame = self.cap.read()	# next frameに進む(read前に？)
		if not ret:
			return
		self.setPos(pos)

		# Build the input data
		frame = cv2.resize(frame, (imageWidth, imageHeight))
		frame = frame.transpose(2, 0, 1)	# [[Redの配列],[Greenの配列],[Blueの配列]] に並び替える

		data = frame.flatten().astype(np.float32)/255.0
		data_size = imageWidth * imageHeight * imageChannel

		eval_data = np.empty((0,data_size))
		eval_labels = np.empty(1, dtype=np.int32)

		eval_data = np.append(eval_data, np.array(data).reshape(1, data_size), axis=0).astype(np.float32)	# row方向に１つ追加

		type = self.getTypeByPosFromMarkList(pos)
		eval_labels[0] = type

		print("type=", type)
#		print("eval_data=", eval_data.shape)

		eval_results = self.predict_model(eval_data, eval_labels)

		#print(eval_results)
		for res in eval_results:
			print(res)
		pr = res['probabilities']
		print("1=%d 2=%d" % (int(pr[1]*100), int(pr[2]*100)))

		frame = cv2.cvtColor(frame, cv2.COLOR_BGR2RGB)

	def calcNumList(self):
		numlist = []
		prev_pos = -1
		for pos in self.PosList:
			if prev_pos != -1:
				numlist.append(pos - prev_pos)
			prev_pos = pos
		if prev_pos == -1:
			prev_pos = 0
		numlist.append(self.getFrameCount() - prev_pos)
		return numlist

	def saveModel(self, filename):
		self.model.save(filename)

	def printModel(self, model_filename=None):
		self.initLearn(model_filename)

		print("Output to model.png")
		plot_model(self.model, to_file='model.png',show_shapes=True)

	#
	# Original dataの表示
	#
	def viewOriginal(self):

		numlist = self.calcNumList()
		count = 0
		start_pos = 0
		end = False

		start_time = time.time()

		for pos, numframe in zip(self.PosList, numlist):
			if end:
				break
			type = self.TypeList[pos]
			SKIP_FRAMES = 3
			for frame_index in range(numframe):
				if frame_index % SKIP_FRAMES != 0:
					continue

				if maxSec != 0 and pos + frame_index > maxSec * self.fps():
					end = True
					break

				self.setPos(pos + frame_index)
				ret, frame = self.cap.read()	# getPosが１つ進む
				if not ret:
					break

				# Build the input data
				frame = cv2.resize(frame, (imageWidth, imageHeight))

				data = frame.flatten().astype(np.float32)/255.0
				data_size = imageWidth * imageHeight * imageChannel

				if count == 0:
					train_data = np.empty((0, data_size))
					train_labels = np.empty(INPUT_SIZE, dtype=np.int32)
					start_pos = pos + frame_index

				train_data = np.append(train_data, np.array(data).reshape(1, data_size), axis=0).astype(np.float32)	# row方向に１つ追加

				train_labels[count] = type
#				print("type=%d pos=%d" % (type, pos + frame_index))

#				print("count=", count)

				count += 1

				if count == INPUT_SIZE:
					start_tc = pos2str(start_pos)
#					print("train_data=", train_data.shape)
#					print("train_labels=", train_labels)
#					print("train_labels=", train_labels.shape)
					curpos = pos + frame_index
					str = "--- Training: %s %d - %d (%d step)" % (start_tc, start_pos, curpos, SKIP_FRAMES)
					logger.info(str)
					end = True
					break

		x_train = train_data
		y_train = train_labels

		if False:
			fig, ax = plt.subplots(nrows=2, ncols=3, sharex=True, sharey=True)
			ax = ax.flatten()

			for i in (1,2):
				img = x_train[y_train == i][0].reshape(imageWidth, imageHeight, imageChannel)
				ax[i].imshow(img, cmap='Greys')

			ax[0].set_xticks([])
			ax[0].set_yticks([])
			plt.tight_layout()
			plt.show()
		else:
			flg, ax = plt.subplots(nrows=5, ncols=5, sharex=True, sharey=True)
			ax = ax.flatten()
			for i in range(25):
				img = x_train[y_train == 1][i].reshape(imageWidth, imageHeight, imageChannel)
				ax[i].imshow(img, cmap='Greys')

			ax[0].set_xticks([])
			ax[0].set_yticks([])
			plt.tight_layout()
			plt.show()

# -------------------------
#	source image情報

	def fps(self):
		return int(self.cap.get( cv2.CAP_PROP_FPS ))

	def getPos(self):
		pos = self.cap.get( cv2.CAP_PROP_POS_FRAMES )
		return int(pos)

	def setPos(self, pos):
		self.cap.set( cv2.CAP_PROP_POS_FRAMES, pos )

	def getFrameCount(self):
		return int(self.cap.get( cv2.CAP_PROP_FRAME_COUNT ))

def pos2str(pos):
	return '%02d:%02d:%02d:%02d' % (int(pos/(3600*30)),int(pos/(60*30)%60), int(pos/30%60), int(pos%30))

import argparse


def main():
	parser = argparse.ArgumentParser(description='gui.pyで作ったデータでkerasを使って計算するよ！')
	parser.add_argument('mode', help='train | train_eval | pred | eval | view')
	parser.add_argument('-n', '--target_number', default='1', help='target number')
	parser.add_argument('-d', '--duration', default=0, help='max duration(second)')
	parser.add_argument('-s', '--save', help='save model [filename] after training')
	parser.add_argument('-l', '--load', help='load model [filename] before training/evaluation')
	parser.add_argument('-sf', '--skip_frames', help='skip frame for input data')
	
	# data loadにすごく時間がかかるので。-d を指定しない場合に限ったほうがいい（混乱するので）
	parser.add_argument('-sd', '--save_data', help='save training data [filename]')
	parser.add_argument('-ld', '--load_data', help='load training data [filename]')

	args = parser.parse_args()

	global TargetFileNumber
	if args.target_number:
		TargetFileNumber = str(args.target_number)
	global maxSec
	if args.duration:
		maxSec = int(args.duration)

	global SKIP_FRAMES
	if args.skip_frames:
		SKIP_FRAMES = int(args.skip_frames)

	global saveDataFileName
	saveDataFileName = args.save_data
	global loadDataFileName
	loadDataFileName = args.load_data

	logger.info(" --- Start --- %s %s" % (args.mode, TargetFileNumber))

	app = MyApp()

	if args.mode == 'view':
		app.viewOriginal()
	elif args.mode == 'train':
		app.trainStart(model_filename=args.load, with_eval=False)
		if args.save:
			app.saveModel(args.save)
	elif args.mode == 'train_eval':
		app.trainStart(model_filename=args.load, with_eval=True)
		if args.save:
			app.saveModel(args.save)
	elif args.mode == 'eval':
		app.eval(filename=args.load)
	elif args.mode == 'pred':
		app.predictAll(filename=args.load)
	elif args.mode == 'model':
		app.printModel(model_filename=args.load)
	else:
		parser.usage()

if __name__ == '__main__':
	main()

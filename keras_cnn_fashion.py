#
# Ref.
# https://recipe.kc-cloud.jp/archives/13029
# typoがある。止めたほうがいいかも
#

#import tensorflow
#from tensorflow import keras

import keras
import matplotlib.pyplot as plt
#%matplotlib inline

(x_train,y_train),(x_test,y_test) = keras.datasets.fashion_mnist.load_data()

x_train = x_train.reshape(x_train.shape[0],28,28,1)
x_test= x_test.reshape(x_test.shape[0],28,28,1)

x_train, x_test = x_train/255.0, x_test/255.0

"""
・33のフィルタを32種使用して、28281の入力データを262632にします。活性化関数には relu を使用します。
・33のフィルタを64種使用して、262632の入力データを242464にします。活性化関数には relu を使用します。
・242464の入力データを121264にします。
・入力を25%Dropout(無視)します。
・2次元の入力データを1次元にします。
・128個のノードに全結合します。活性化関数には relu を使用します。
・入力を50%Dropout(無視)します。
・10個のノードに全結合します。活性化関数には softmax を使用します。
"""

model = keras.models.Sequential([
  keras.layers.Conv2D(32,kernel_size = (3,3),
                        activation = "relu",
                        input_shape = (28,28,1)),
  keras.layers.Conv2D(64,(3,3),activation = "relu"),
  keras.layers.MaxPooling2D(pool_size=(2,2)),
  keras.layers.Dropout(0.25),
  keras.layers.Flatten(),
  keras.layers.Dense(128,activation="relu"),
  keras.layers.Dropout(0.5),
  keras.layers.Dense(10,activation = "softmax")
])


model.compile(optimizer="adam",loss="sparse_categorical_crossentropy",metrics=["accuracy"])

model.fit(x_train,y_train,epochs = 10)

model.evaluate(x_test,y_test)

model.predict(x_test)

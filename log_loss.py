#!/usr/bin/python3
#
#

import sys

args = sys.argv

file = 'keras_cnn.log'

if len(args) >= 2:
	file = args[1]

with open(file) as f:
	for line in f:
		if 'loss' in line:
			print(line[49:], end='')

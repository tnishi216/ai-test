
import sys
import re

col = 0
for line in sys.stdin:
	res = re.match('.*loss=([\.\d]+).+accuracy=([\.\d]+)', line)
	if res:
		col += 1
		if col>1:
			print('\t', end="")
		print(res.group(1), end="")
		if col==8:
			print('\t'+res.group(2))
			col = 0



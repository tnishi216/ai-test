#
#
# File Format:
#frame number	type(0-)
#

class MarkList:
	def __init__(self):
		self.clear()

	def clear(self):
		self.frames = []
		self.types = {}

	def load(self, filename):
		self.clear()
		try:
			for line in open(filename, 'r'):
				if line[0] == '#':
					continue
				frame, type = line[:-1].split('\t')
				if frame != "" and type != "":
					frame = int(frame)
					self.frames.append(frame)
					self.types[frame] = int(type)
			return True
		except:
			print("Failed to load : ", filename)
			return False

	def save(self, filename):
		with open(filename, "w") as f:
			for frame in self.frames:
				f.write(str(frame) + "\t" + str(self.types[frame]) + "\n")

	def print(self):
		print(self.frames)
		print(self.types)

def main():
	marklist = MarkList()
	marklist.load('marklist.txt')
	marklist.print()

#	marklist.save('test-out.tsv')

if __name__ == '__main__':
	main()

#
# Refer
# https://www.pyimagesearch.com/2014/09/15/python-compare-two-images/
# これを元に複数のmp4ファイルの違い(mse,ssim)を計算

# import the necessary packages
#from skimage.measure import structural_similarity as ssim
#from skimage import measure
from skimage.metrics import structural_similarity as ssim
import matplotlib.pyplot as plt
import numpy as np
import cv2

def mse(imageA, imageB):
	# the 'Mean Squared Error' between the two images is the
	# sum of the squared difference between the two images;
	# NOTE: the two images must have the same dimension
	err = np.sum((imageA.astype("float") - imageB.astype("float")) ** 2)
	err /= float(imageA.shape[0] * imageA.shape[1])
	
	# return the MSE, the lower the error, the more "similar"
	# the two images are
	return err

def compare_images(imageA, imageB, title=None):
	# compute the mean squared error and structural similarity
	# index for the images
	m = mse(imageA, imageB)
	s = ssim(imageA, imageB, multichannel=True)

	if title != None:
		# setup the figure
		fig = plt.figure(title)
		plt.suptitle("MSE: %.2f, SSIM: %.2f" % (m, s))

		# show first image
		ax = fig.add_subplot(1, 2, 1)
		plt.imshow(imageA, cmap = plt.cm.gray)
		plt.axis("off")

		# show the second image
		ax = fig.add_subplot(1, 2, 2)
		plt.imshow(imageB, cmap = plt.cm.gray)
		plt.axis("off")

		# show the images
		plt.show()
#	else:
#		print("mse={} ssim={}".format(m, s))
	return m, s

def demo():
	# load the images -- the original, the original + contrast,
	# and the original + photoshop
	original = cv2.imread("image/test1.jpg")
	contrast = cv2.imread("image/test2.jpg")
	shopped = cv2.imread("image/test3.jpg")
	 
	# convert the images to grayscale
	#original = cv2.cvtColor(original, cv2.COLOR_BGR2GRAY)
	#contrast = cv2.cvtColor(contrast, cv2.COLOR_BGR2GRAY)
	#shopped = cv2.cvtColor(shopped, cv2.COLOR_BGR2GRAY)

	# initialize the figure
	fig = plt.figure("Images")
	images = ("Original", original), ("Contrast", contrast), ("Photoshopped", shopped)

	# loop over the images
	for (i, (name, image)) in enumerate(images):
		# show the image
		ax = fig.add_subplot(1, 3, i + 1)
		ax.set_title(name)
		plt.imshow(image)
		plt.axis("off")

	# show the figure
	plt.show()

	# compare the images
	compare_images(original, original, "Original vs. Original")
	compare_images(original, contrast, "Original vs. Contrast")
	compare_images(original, shopped, "Original vs. Photoshopped")

def read_frames(filename, count):
	cap = cv2.VideoCapture(filename)
	if cap == None:
		print("open error!!", filename)
		exit(1)
	width = int(cap.get(cv2.CAP_PROP_FRAME_WIDTH))
	height = int(cap.get(cv2.CAP_PROP_FRAME_HEIGHT))
	frames = []
	for i in range(count):
		ret, frame = cap.read()
		if not ret:
			print("read error!")
			return
		frames.append(frame)
	return frames

dir = "V:/cap/ama/"

prefix = dir + "amarecco20111222-031059[000]-"

bitrates = ['4000-6000', '2000-2500', '1800-2000', '1600-2000', '1500-2000', '1400-2000', '1200-1600', '1000-1500', '900-1400', '800-1200', '700-1200', '600-1200', '500-1000', '400-1000', '300-1000', '200-1000', '100-1000']

orgrate = bitrates[0]
orgfilename = prefix + orgrate + '.wmv'

filenames = []
for bitrate in bitrates:
	filename = prefix + bitrate + '.wmv'
#	print("filename=",filename)
	filenames.append(filename)

num_frames = 10

filenames = []
filenames.append(dir + "amarecco20111028-091051[000].avi")

orgframes = read_frames(orgfilename, num_frames)

exit()

for filename in filenames:
	print("--- filename=", filename)
	frames = read_frames(filename, num_frames)
	mse_sum = 0
	ssim_sum = 0
	for i in range(len(frames)):
		_mse, _ssim = compare_images(orgframes[i], frames[i])
		mse_sum += _mse
		ssim_sum += _ssim
	print("avg={} {}".format(mse_sum/len(frames), ssim_sum/len(frames)))

'''
http://agehatype0.blog50.fc2.com/blog-entry-181.html
より
0.98以上 オリジナルと区別がつかない
0.95     見るに堪えない
0.9      非常に酷い
0.8      これ以上圧縮出来ない

	bitrate	SSIM	size	大きい方が良い	'=size/bitrate
2000/2500	2000	1.0	15941	
1800/2000	1800	0.96108	14405	66.7185005206526	8.00277777777778
1600/2000	1600	0.96108	12917	74.4042734381048	8.073125
1500-2000	1500	0.94843	12133	78.1694552048133	8.08866666666667
1400-2000	1400	0.94843	11397	83.2175133807142	8.14071428571429
1200-1600	1200	0.93957	9877	95.1270628733421	8.23083333333333
1000-1500	1000	0.9317	8357	111.487375852579	8.357
900-1400	900	0.9317	7621	122.254297336308	8.46777777777778
800-1200	800	0.92915	6853	135.582956369473	8.56625
700-1200	700	0.91656	6085	150.626129827445	8.69285714285714
600-1200	600	0.89909	5333	168.589911869492	8.88833333333333
500-1000	500	0.88249	4565	193.316538882804	9.13
400-1000	400	0.86752	3813	227.516391292945	9.5325
300-1000	300	0.83956	3061	274.276380267886	10.2033333333333
200-1000	200	0.78851	2578	305.861132660977	12.89
100-1000	100	0.72766	7262	100.201046543652	72.62
'''

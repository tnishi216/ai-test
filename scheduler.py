from glob import glob
import subprocess
import time
import os

while True:
	files = glob('task*')
	tasks = []
	for file in files:
		if not '.done' in file:
			tasks.append(file)

	if len(tasks) == 0:
		time.sleep(1)
		continue

	print(tasks)

	tasks.sort()

	print(tasks)

	exit()

	for task in tasks:
		with open('cur_task', mode='w') as f:
			f.write(task)

		if not task.startswith('./'):
			task = './' + task

		print("Process: ", task)
		subprocess.call([task], shell=True)

		# task -> task.done
		done = task + '.done'
		if os.path.exists(done):
			os.remove(done)
		os.rename(task, done)
		os.remove('cur_task')

